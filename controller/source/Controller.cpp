//
// Created by pawel on 01.08.2021.
//

#include "../header/Controller.h"
#include "../../model/header/Camera.h"
#include "../../configs/defines.h"
#include "../../view/header/Screen.h"
#include "../../model/header/World.h"
#include "../../model/perlin/header/events/LeftMouseButtonClickEvent.h"
#include "../../model/perlin/header/events/RightMouseButtonClickEvent.h"
#include "../../model/perlin/header/events/NextBlockButtonClickEvent.h"
#include "../../model/perlin/header/events/PreviousBlockButtonClickEvent.h"

#ifndef GLFW_H
#define GLFW_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/vec3.hpp>
#include <glm/geometric.hpp>
#endif //GLFW_H

Controller *Controller::controller = 0;
bool Controller::window_focused = true;
Screen *Controller::screen = 0;
World *Controller::world = 0;
ClickData Controller::leftClick = {false, false};
ClickData Controller::rightClick = {false, false};
bool Controller::singlePressSwitchBlock = false;

Controller *Controller::getInstance() {
    if (!controller)
        [[unlikely]]
                controller = new Controller();
    return controller;
}

void Controller::processInput(GLFWwindow *window) {

    if (window_focused) {

        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
            Camera::getInstance()->walk(FORWARD);
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
            Camera::getInstance()->walk(BACKWARD);
        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
            Camera::getInstance()->walk(LEFT);
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
            Camera::getInstance()->walk(RIGHT);

        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
            Camera::getInstance()->walk(UP);
        if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
            Camera::getInstance()->walk(DOWN);

        if (!singlePressSwitchBlock) {

            if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS) {
                singlePressSwitchBlock = true;
                world->eventQueue.enqueue(new NextBlockButtonClickEvent());
            } else if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
                singlePressSwitchBlock = true;
                world->eventQueue.enqueue(new PreviousBlockButtonClickEvent());
            }
        } else {
            if (glfwGetKey(window, GLFW_KEY_N) == GLFW_RELEASE && glfwGetKey(window, GLFW_KEY_P) == GLFW_RELEASE) {
                singlePressSwitchBlock = false;
            }
        }

        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            glfwSetCursorPosCallback(window, nullptr);
            window_focused = false;
        }
    }
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
        leftClick.old = leftClick.current;
        leftClick.current = true;
        if (!window_focused) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            glfwSetCursorPosCallback(window, Camera::mouse_callback);
            window_focused = true;
        } else if (leftClick.old ^ leftClick.current) {
            world->eventQueue.enqueue(new LeftMouseButtonClickEvent());
        }
    } else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE) {
        leftClick.old = leftClick.current;
        leftClick.current = false;
    }
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
        rightClick.old = rightClick.current;
        rightClick.current = true;
        if (!window_focused) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            glfwSetCursorPosCallback(window, Camera::mouse_callback);
            window_focused = true;
        } else if (rightClick.old ^ rightClick.current) {
            world->eventQueue.enqueue(new RightMouseButtonClickEvent());
        }
    } else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE) {
        rightClick.old = rightClick.current;
        rightClick.current = false;
    }


}

void Controller::framebuffer_size_callback(GLFWwindow *window, int width, int height) {
    if (screen != 0) {
        screen->setHeight(height);
        screen->setWidth(width);
    }

    int size = height > width ? height : width;
    glViewport(0, 0, size, size);
}

void Controller::bindScreen(Screen *screen) {
    Controller::screen = screen;
}

Controller::Controller() {

}

Controller::~Controller() {

}

void Controller::bindWorld(World *world) {
    Controller::world = world;
}
