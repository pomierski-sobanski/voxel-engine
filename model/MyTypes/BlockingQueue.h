//
// Created by pawel on 04.08.2021.
//

#ifndef VOXEL_ENGINE_BLOCKINGQUEUE_H
#define VOXEL_ENGINE_BLOCKINGQUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

///Given type requires default constructor
template<typename T>
class BlockingQueue {
public:
    BlockingQueue() = default;

    void quitOnExit() {
        std::scoped_lock lock(mutex);
        queue.push(T());
        cv.notify_one();
    }

    void quitOnExit(int val) {
        std::scoped_lock lock(mutex);
        queue.push(T(val));
        cv.notify_one();
    }

    void enqueue(T value) {
        std::scoped_lock lock(mutex);
        queue.push(value);
        cv.notify_one();
    }

    auto dequeue() {
        std::scoped_lock lock(mutex);
        auto first = queue.front();
        queue.pop();
        return first;
    }

    ///Dequeue and sleep
    auto dequeueAndWait() {
        std::unique_lock<std::mutex> lock(mutex);
        cv.wait(lock, [&]() { return !queue.empty(); });
        auto first = queue.front();
        queue.pop();
        return first;
    }

    bool empty() {
        return queue.empty();
    }

private:
    std::mutex mutex;
    std::queue<T> queue;
    std::condition_variable cv;
};

#endif //VOXEL_ENGINE_BLOCKINGQUEUE_H
