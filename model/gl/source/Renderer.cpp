//
// Created by pawel on 11.07.2021.
//

#include "../header/Renderer.h"
#include "../header/VertexArray.h"
#include "../header/IndexBuffer.h"
#include "../header/ShaderProgram.h"

Renderer *Renderer::renderer = 0;

Renderer *Renderer::getInstance() {
    if (!renderer)
        [[unlikely]]
                renderer = new Renderer();
    return renderer;
}


Renderer::Renderer() {

}

void Renderer::draw(const VertexArray &va, const IndexBuffer &ib, const ShaderProgram &program) const {
    program.use();
    va.bind();
    ib.bind();

    glDrawElements(GL_TRIANGLES, ib.getCount(), GL_UNSIGNED_INT, nullptr);
}

void Renderer::draw(const VertexArray &va, const IndexBuffer &ib, unsigned count, const ShaderProgram &program) const {
    program.use();
    va.bind();
    ib.bind();

    glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, nullptr);
}

void Renderer::draw(const VertexArray &va, unsigned int count, const ShaderProgram &program) const {
    program.use();
    va.bind();
    IndexBuffer::unbind();
    glDrawArrays(GL_TRIANGLES, 0, count);
}


void Renderer::clear() const {
    glClearColor(0.53f, 0.8f, 0.92f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

Renderer::~Renderer() {

}