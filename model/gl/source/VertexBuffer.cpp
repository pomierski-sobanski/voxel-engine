//
// Created by pawel on 11.07.2021.
//

#include "../header/VertexBuffer.h"
#include "../header/Renderer.h"

VertexBuffer::VertexBuffer(const void *data, unsigned int elemSize, unsigned int elemCount, unsigned int mode) :
        elemSize(elemSize) {
    this->elemCount = elemCount;
    glGenBuffers(1, &rendererID);
    glBindBuffer(GL_ARRAY_BUFFER, rendererID);
    glBufferData(GL_ARRAY_BUFFER, elemSize * elemCount, data, mode);
}

void VertexBuffer::bind() const {
    glBindBuffer(GL_ARRAY_BUFFER, rendererID);
}

void VertexBuffer::unbind() {
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

VertexBuffer::~VertexBuffer() {
    glDeleteBuffers(1, &rendererID);
}

unsigned int VertexBuffer::getElemSize() const {
    return elemSize;
}

unsigned int VertexBuffer::getElemCount() const {
    return elemCount;
}

unsigned int VertexBuffer::getUsedMeory() const {
    return elemCount * elemSize;
}