//
// Created by pawel on 11.07.2021.
//

#include "../header/VertexBufferLayout.h"

VertexBufferLayout::VertexBufferLayout() : stride(0) {};

unsigned int VertexBufferLayout::getStride() const {
    return stride;
}

VertexBufferLayout::~VertexBufferLayout() {

}
