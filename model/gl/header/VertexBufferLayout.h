//
// Created by pawel on 11.07.2021.
//
#ifndef VOXEL_ENGINE_VERTEXBUFFERLAYOUT_H
#define VOXEL_ENGINE_VERTEXBUFFERLAYOUT_H

#include "Renderer.h"
#include <vector>
#include <cassert>

template<typename T>
struct identity {
    typedef T type;
};

struct VertexBufferElements {
    unsigned int type;
    unsigned int count;
    bool normalized;

    static unsigned int getSizeofType(unsigned int type) {
        switch (type) {
            case GL_FLOAT:
                return sizeof(GLfloat);
            case GL_UNSIGNED_INT:
                return sizeof(GLuint);
            case GL_UNSIGNED_BYTE:
                return sizeof(GLubyte);
        }
        assert(false);
    }
};

class VertexBufferLayout {
public:
    VertexBufferLayout();

    template<typename T>
    void push(unsigned int count) {
        push(count, identity<T>());
    }

    inline const std::vector<VertexBufferElements> getElements() const { return elements; }

    unsigned int getStride() const;

    ~VertexBufferLayout();

private:
    std::vector<VertexBufferElements> elements;
    unsigned int stride;

    template<typename T>
    void push(unsigned int count, identity<T>) {
        assert(false);
    }

    void push(unsigned int count, identity<float>) {
        elements.push_back({GL_FLOAT, count, false});
        stride += count * sizeof(GLfloat);
    }

    void push(unsigned int count, identity<unsigned int>) {
        elements.push_back({GL_UNSIGNED_INT, count, false});
        stride += count * sizeof(GLuint);
    }

    void push(unsigned int count, identity<unsigned char>) {
        elements.push_back({GL_UNSIGNED_BYTE, count, true});
        stride += count * sizeof(GLubyte);
    }

};


#endif //VOXEL_ENGINE_VERTEXBUFFERLAYOUT_H
