//
// Created by pawel on 11.07.2021.
//
#ifndef VOXEL_ENGINE_VERTEXARRAY_H
#define VOXEL_ENGINE_VERTEXARRAY_H


#include "VertexBufferLayout.h"
#include "VertexBuffer.h"

class VertexArray {
public:
    VertexArray();

    void addBuffer(const VertexBuffer &vb, const VertexBufferLayout &layout);

    void bind() const;

    static void unbind();

    ~VertexArray();

private:
    unsigned int renderer;
};


#endif //VOXEL_ENGINE_VERTEXARRAY_H
