//
// Created by pawel on 26.07.2021.
//

#ifndef VOXEL_ENGINE_SHADERPROGRAM_H
#define VOXEL_ENGINE_SHADERPROGRAM_H

#include <string>
#include <glad/glad.h>

class Shader;

class ShaderProgram {
public:
    ShaderProgram();

    void attach(Shader &shader) const;

    void link() const;

    void use() const;

    static void detach();

    void deleteProgram() const;

    unsigned int getProgram() const;

    void setUniform1i(const std::string &name, int v0);

    void setUniform4f(const std::string &name, float v0, float v1, float v2, float v3);

    void setUniform3f(const std::string &name, float v0, float v1, float v2);

    void setUniformMatrix4fv(const std::string &name, const GLfloat *value);

    int getUniformLocation(const std::string &name);

    ~ShaderProgram();

private:
    unsigned int program;

};


#endif //VOXEL_ENGINE_SHADERPROGRAM_H
