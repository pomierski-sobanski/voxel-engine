//
// Created by pawel on 11.07.2021.
//
#include <string>

#ifndef VOXEL_ENGINE_TEXTURE_H
#define VOXEL_ENGINE_TEXTURE_H


class Texture {
public:
    Texture(const std::string &path);

    void bind(unsigned int slot = 0) const;

    static void unbind();

    ~Texture();

private:
    unsigned int texture;
    std::string filePath;
    unsigned char *localBuffer;
    int width, height, bbp;
public:
    int getWidth() const;

    void setWidth(int width);

    int getHeight() const;

    void setHeight(int height);

    int getBbp() const;

    void setBbp(int bbp);
};


#endif //VOXEL_ENGINE_TEXTURE_H
