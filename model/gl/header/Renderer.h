//
// Created by pawel on 11.07.2021.
//
#ifndef VOXEL_ENGINE_RENDERER_H
#define VOXEL_ENGINE_RENDERER_H

#ifndef GLFW_H
#define GLFW_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#endif //GLFW_H

class VertexArray;

class IndexBuffer;

class ShaderProgram;

class Renderer {
public:
    static Renderer *getInstance();

    void draw(const VertexArray &va, const IndexBuffer &ib, const ShaderProgram &program) const;

    void draw(const VertexArray &va, unsigned int count, const ShaderProgram &program) const;

    void draw(const VertexArray &va, const IndexBuffer &ib, unsigned count, const ShaderProgram &program) const;

    void clear() const;

private:

    Renderer();

    ~Renderer();

    static Renderer *renderer;
};


#endif //VOXEL_ENGINE_RENDERER_H
