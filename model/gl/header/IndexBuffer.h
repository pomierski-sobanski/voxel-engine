//
// Created by pawel on 11.07.2021.
//
#ifndef VOXEL_ENGINE_INDEXBUFFER_H
#define VOXEL_ENGINE_INDEXBUFFER_H

class IndexBuffer {
public:
    IndexBuffer(const unsigned int *data, unsigned int count);

    void bind() const;

    static void unbind();

    unsigned int getCount() const;

    ~IndexBuffer();

private:
    unsigned int rendererID;
    unsigned int count;
};


#endif //VOXEL_ENGINE_INDEXBUFFER_H
