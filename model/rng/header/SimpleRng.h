//
// Created by Michał Sobański on 12.09.2021.
//

#ifndef VOXEL_ENGINE_SIMPLERNG_H
#define VOXEL_ENGINE_SIMPLERNG_H

#define MODULUS ((((uint64_t)1) << 31) - 1)
#define MULTIPLIER 1103515245
#define INCREMENT 12345

#include <cstdint>

class SimpleRng {
private:
    void rand();

    uint64_t seed = 0;

public:
    explicit SimpleRng(uint64_t seed);

    double randDouble();

    double randDouble(double min, double max);

    uint64_t randInt(uint64_t min, uint64_t max);

    bool randBool();

    bool withProb(double probability);
};


#endif //VOXEL_ENGINE_SIMPLERNG_H
