//
// Created by pawel on 06.07.2021.
//

#ifndef VOXEL_ENGINE_WORLD_H
#define VOXEL_ENGINE_WORLD_H

#include "../perlin/header/Perlin.h"
#include "../perlin/header/events/PerlinEvent.h"
#include "../gl/header/Renderer.h"
#include "../MyTypes/BlockingQueue.h"
#include "Camera.h"
#include "../../view/header/Screen.h"
#include "../header/ChunksData.h"
#include <cstdint>
#include <atomic>

class Camera;

class ChunksData;

class World {
public:
    World(Screen *screen, uint32_t seed);

    int start();

    virtual ~World();

    uint32_t getSeed() const;

    BlockingQueue<PerlinEvent *> eventQueue;
    BlockingQueue<ChunkId> perlinInput;
    BlockingQueue<PerlinChunk *> perlinOutput;
    std::atomic<bool> running;

private:
    const uint32_t seed;

    void draw();

    std::vector<ChunkId> getNewLandscape();

    void manageChunksAndRequestNew();

    void updatePerlinCameraInfo();

    void updateWorld();

    Screen *screen;
    Perlin *perlin;

    ChunksData *chunks;
};

//                   DISTANCE = 5
//      *   *   *   *   *   *   *   *   *   *   *
//      *   *   *   *   *   *   *   *   *   *   *
//      5   5   5   5   5   5   5   5   5   5   5
//      5   4   4   4   4   4   4   4   4   4   5
//      5   4   3   3   3   3   3   3   3   4   5
//      5   4   3   2   2   2   2   2   3   4   5
//      5   4   3   2   1   1   1   2   3   4   5
//      5   4   3   2   1  ME   1   2   3   4   5
//      5   4   3   2   1   1   1   2   3   4   5
//      5   4   3   2   2   2   2   2   3   4   5
//      5   4   3   3   3   3   3   3   3   4   5
//      5   4   4   4   4   4   4   4   4   4   5
//      5   5   5   5   5   5   5   5   5   5   5
//      *   *   *   *   *   *   *   *   *   *   *
//      *   *   *   *   *   *   *   *   *   *   *
//
//      No. of chunk is equal (2*distance + 1)^2

#endif //VOXEL_ENGINE_WORLD_H
