//
// Created by pawel on 05.09.2021.
//

#ifndef VOXEL_ENGINE_CHUNKSDATA_H
#define VOXEL_ENGINE_CHUNKSDATA_H


#include <glad/glad.h>
#include <map>
#include "../gl/header/ShaderProgram.h"
#include "Chunk.h"

class Chunk;

class ChunksData {
public:
    ChunksData();

    ~ChunksData();

    void resize(int size);

    void prepareIndexBuffer();

    void prepareShaderProgram() const;

    void addChunk(ChunkId chunkId);

    void freeChunk(ChunkId chunkId);

    void loadNew(PerlinChunk *perlinChunk);

    void checkForReload();

    void printData();

    GLuint *indices;
    ShaderProgram shaderProgram;
    IndexBuffer *ib;
    Chunk **chunks;
    std::map<ChunkId, int> chunkMap;

    void organizeInOrder();
};


#endif //VOXEL_ENGINE_CHUNKSDATA_H
