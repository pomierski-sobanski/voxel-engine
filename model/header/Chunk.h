//
// Created by pawel on 06.07.2021.
//

#ifndef VOXEL_ENGINE_CHUNK_H
#define VOXEL_ENGINE_CHUNK_H

#include "World.h"

#include "../gl/header/VertexArray.h"
#include "../gl/header/IndexBuffer.h"

class Chunk {
public:
    Chunk();

    void setWorld(World *world);

    const ChunkId &getChunkId() const;

    void setChunkId(const ChunkId &chunkId);

    void loadNew(PerlinChunk *perlinChunk);

    bool reloadRequired() const;

    void draw_solid();

    void draw_transparent();

    void setIb(IndexBuffer *ib);

    void setSp(ShaderProgram *sp);

    bool isUsed();

    void use();

    void freeChunk();

    int getUsedMemory() const;

    PerlinChunk *perlinChunk;

    ~Chunk();

    void load();

private:
    bool alreadyLoaded;
    bool used;
    ChunkId chunkId;
    unsigned faceCounter_solid;
    unsigned faceCounter_transparent;
    VertexArray *va_solid;
    VertexBuffer *vb_solid;

    VertexArray *va_transparent;
    VertexBuffer *vb_transparent;

    VertexBufferLayout layout;

    IndexBuffer *ib;
    ShaderProgram *sp;

};


#endif //VOXEL_ENGINE_CHUNK_H
