//
// Created by pawel on 06.07.2021.
//

#ifndef VOXEL_ENGINE_CAMERA_H
#define VOXEL_ENGINE_CAMERA_H

#include "World.h"
#include <glm/vec3.hpp>
#include <glm/matrix.hpp>

class World;

class Camera {
public:
    static Camera *getInstance(GLFWwindow *window = nullptr);

    static void mouse_callback(GLFWwindow *window, double xpos, double ypos);

    void walk(unsigned int direction);

    glm::mat4 update();

    glm::vec3 getDirection();

    glm::vec3 &getPos();

    glm::vec3 &getFront();

    glm::vec3 &getUp();

    ChunkId getChunkID();

    ChunkId getPrevChunkID();

    bool chunkChanged();

    bool positionChanged();

    bool lookDirChanged();

private:
    explicit Camera(GLFWwindow *window = nullptr);

    ~Camera();

    static Camera *camera;
    const GLFWwindow *window;

    typedef struct PositionInfo {
        glm::vec3 cameraPos;
        glm::vec3 prevCameraPos;

        ChunkId chunkId;
        ChunkId prevChunkId;
    } PositionInfo;

    static PositionInfo positionInfo;

    static glm::vec3 prevCameraFront;
    static glm::vec3 cameraFront;
    static glm::vec3 cameraUp;

    void updateChunkId();

    static bool firstMouse;

    static float yaw;
    static float pitch;

    static float lastX;
    static float lastY;

    static float cameraSpeed;
    static float sensitivity;

    static float deltaTime;
    static float previousTime;


    void updateTime(double time) const;

    void updatePosition() const;

    void updateLookDir();

};


#endif //VOXEL_ENGINE_CAMERA_H
