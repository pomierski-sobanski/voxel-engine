//
// Created by Michał Sobański on 25.08.2021.
//

#include "../header/JsonConfig.h"

JsonConfig *JsonConfig::jsonConfig = nullptr;

JsonConfig::JsonConfig() {
    readConfigFile();
}

JsonConfig *JsonConfig::getInstance() {
    if (!jsonConfig) {
        [[unlikely]]
                jsonConfig = new JsonConfig();
    }
    return JsonConfig::jsonConfig;
}

void JsonConfig::readConfigFile() {
    std::unique_lock<std::mutex> lock(ioMutex);
    std::ifstream inputStream(CONFIG_FILE_PATH);
    if (!inputStream.is_open()) {
        std::cerr << "Error opening config file: " << CONFIG_FILE_PATH << std::endl;
        return;
    }
    inputStream >> json;
    inputStream.close();
}

void JsonConfig::writeToConfigFile() {
    std::unique_lock<std::mutex> lock(ioMutex);
    std::ofstream outputStream(CONFIG_FILE_PATH);
    if (!outputStream.is_open()) {
        std::cerr << "Error opening config file: " << CONFIG_FILE_PATH << std::endl;
        return;
    }
    outputStream << std::setw(JSON_INDENTATION) << json;
    outputStream.close();
}

void JsonConfig::print() {
    std::unique_lock<std::mutex> lock(objMutex);
    std::cout << json.dump(JSON_INDENTATION) << std::endl;
}

uint8_t JsonConfig::getRenderDistance() {
    return getValue<uint8_t>(RENDER_DIST_JSON_KEY);
}

float JsonConfig::getSensitivity() {
    return getValue<float>(SENSITIVITY_JSON_KEY);
}

float JsonConfig::getSpeed() {
    return getValue<float>(SPEED_JSON_KEY);
}

float JsonConfig::getFov() {
    return getValue<float>(FOV_JSON_KEY);
}

std::string JsonConfig::getTitle() {
    return getValue<std::string>(TITLE_JSON_KEY);
}

uint32_t JsonConfig::getHeight() {
    return getValue<uint32_t>(HEIGHT_JSON_KEY);
}

uint32_t JsonConfig::getWidth() {
    return getValue<uint32_t>(WIDTH_JSON_KEY);
}

template<class Type>
Type JsonConfig::getValue(const std::string &jsonKey) {
    std::unique_lock<std::mutex> lock(objMutex);
    return json[jsonKey];
}

void JsonConfig::setRenderDistance(uint8_t renderDistance) {
    setValue<uint8_t>(RENDER_DIST_JSON_KEY, renderDistance);
}

void JsonConfig::setSensitivity(float sensitivity) {
    setValue<float>(SENSITIVITY_JSON_KEY, sensitivity);
}

void JsonConfig::setSpeed(float speed) {
    setValue<float>(SPEED_JSON_KEY, speed);
}

void JsonConfig::setFov(float fov) {
    setValue<float>(FOV_JSON_KEY, fov);
}

void JsonConfig::setTitle(std::string &title) {
    setValue<std::string>(TITLE_JSON_KEY, title);
}

void JsonConfig::setHeight(uint32_t height) {
    setValue<uint32_t>(HEIGHT_JSON_KEY, height);
}

void JsonConfig::setWidth(uint32_t width) {
    setValue<uint32_t>(WIDTH_JSON_KEY, width);
}

template<class Type>
void JsonConfig::setValue(const std::string &jsonKey, const Type &value) {
    std::unique_lock<std::mutex> lock(objMutex);
    json[jsonKey] = value;
    writeToConfigFile();
}
