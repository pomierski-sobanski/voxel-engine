//
// Created by pawel on 06.07.2021.
//

#include "../header/World.h"
#include "../gl/header/Shader.h"
#include "../gl/header/Texture.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "../../controller/header/Controller.h"
#include "../header/Cube.h"
#include "../header/JsonConfig.h"

World::World(Screen *screen, uint32_t seed) : seed(seed), screen(screen), running(true) {
    perlin = new Perlin(this);
    chunks = new ChunksData();
}

World::~World() {
    perlinInput.quitOnExit(); //Wake up perlin thread to join
    eventQueue.quitOnExit();

    delete screen;
    screen = nullptr;

    delete perlin;
    perlin = nullptr;

    delete[] chunks;
    chunks = nullptr;
}

uint32_t World::getSeed() const {
    return seed;
}

int World::start() {
    Camera::getInstance(screen->getWindow());
    Controller::getInstance()->bindScreen(screen);
    Controller::getInstance()->bindWorld(this);

#if DEBUG
    chunks->printData();
#else
#endif
    glfwSetInputMode(screen->getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwSwapInterval(1);

    glfwSetFramebufferSizeCallback(screen->getWindow(), Controller::framebuffer_size_callback);
    glfwSetCursorPosCallback(screen->getWindow(), Camera::mouse_callback);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);

    Texture texture(TEXTURE_DIR + std::string("spritesheet.png"));
    texture.bind();

    VertexArray::unbind();
    VertexBuffer::unbind();
    IndexBuffer::unbind();
    Texture::unbind();
    ShaderProgram::detach();

    manageChunksAndRequestNew();

    while (!glfwWindowShouldClose(screen->getWindow())) {

        Renderer::getInstance()->clear();

        manageChunksAndRequestNew();

        updatePerlinCameraInfo();

        updateWorld();

        chunks->shaderProgram.setUniform1i("u_Texture", 0);
        chunks->shaderProgram.setUniform3f("u_LightDir", 0.2f, 1.0f, 0.3f);
        chunks->shaderProgram.setUniform3f("u_LightColor", 1.0f, 1.0f, 1.0f);

        glm::mat4 model = glm::mat4(1.0f);
        glm::mat4 view = Camera::getInstance()->update();
        glm::mat4 projection = glm::perspective(glm::radians(JsonConfig::getInstance()->getFov()),
                                                1.0f, 0.1f, 100.0f);

        chunks->shaderProgram.setUniformMatrix4fv("u_View", glm::value_ptr(view));
        chunks->shaderProgram.setUniformMatrix4fv("u_Projection", glm::value_ptr(projection));
        chunks->shaderProgram.setUniformMatrix4fv("u_Model", glm::value_ptr(model));

        Controller::getInstance()->processInput(screen->getWindow());

        texture.bind();

        draw();

        glfwSwapBuffers(screen->getWindow());

        glfwPollEvents();
    }
    running.store(false);
    std::cout << "Exiting" << std::endl;
    return 0;
}

void World::draw() {
    int noOfChunks = visibleChunks();
    for (int i = 0; i < noOfChunks; ++i) {
        //std::cout << "Drawing chunk " << i + 1 << "/" << noOfChunks << " solid -> ";
        chunks->chunks[i]->draw_solid();
        //std::cout << " END" << std::endl;
    }
    //std::cout << "Solid drawing ended" << std::endl;
    for (int i = 0; i < noOfChunks; ++i) {
        //std::cout << "Drawing chunk " << i + 1 << "/" << noOfChunks << " transparent -> ";
        chunks->chunks[i]->draw_transparent();
        //std::cout << " END" << std::endl;
    }
}

std::vector<ChunkId> World::getNewLandscape() {
    ChunkId current = Camera::getInstance()->getChunkID();
    std::vector<ChunkId> newLandscape;
    int renderDistance = JsonConfig::getInstance()->getRenderDistance();
    int startX = (static_cast<int>(current.x) - renderDistance) > 0 ? static_cast<int>(current.x) - renderDistance : 0;
    int startY = (static_cast<int>(current.y) - renderDistance) > 0 ? static_cast<int>(current.y) - renderDistance : 0;

    for (uint32_t i = startX; i < startX + (2 * renderDistance + 1); ++i) {
        for (uint32_t j = startY; j < startY + (2 * renderDistance + 1); ++j) {
            newLandscape.push_back(ChunkId({i, j}));
        }
    }
    return newLandscape;
}

void World::manageChunksAndRequestNew() {
    if (!Camera::getInstance()->chunkChanged()) return;

    std::vector<ChunkId> newLandscape = getNewLandscape();

    for (int i = 0; i < visibleChunks(); ++i) {
        if (chunks->chunks[i]->isUsed()) {
            ChunkId chunkId = chunks->chunks[i]->getChunkId();
            auto it = std::find(newLandscape.begin(), newLandscape.end(), chunkId);
            if (it == newLandscape.end()) {
                chunks->chunks[i]->freeChunk();
            } else {
                newLandscape.erase(it);
            }
        }
    }

    for (ChunkId chunkId: newLandscape) {
        chunks->addChunk(chunkId);
        perlinInput.enqueue(chunkId);
    }
}

void World::updateWorld() {
    chunks->checkForReload();
    //if (Camera::getInstance()->chunkChanged() && perlinOutput.empty())
    //std::cout << "Empty queue" << std::endl;
    while (!perlinOutput.empty()) {
        chunks->loadNew(perlinOutput.dequeue());
    }
    //std::cout << "Organizing -> ";
    chunks->organizeInOrder();
    //std::cout << " END" << std::endl;

}

void World::updatePerlinCameraInfo() {
    if (!Camera::getInstance()->positionChanged() && !Camera::getInstance()->lookDirChanged()) return;
    glm::vec3 position = Camera::getInstance()->getPos();
    perlin->setCameraPoint(glm::vec3(position[0], std::abs(position[2]), position[1]));
    glm::vec3 cameraFront = Camera::getInstance()->getFront();
    perlin->setCameraVector(glm::vec3(cameraFront[0], -1 * cameraFront[2], cameraFront[1]));
}
