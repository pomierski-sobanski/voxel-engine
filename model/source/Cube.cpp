//
// Created by pawel on 06.07.2021.
//

#include <string>
#include <utility>
#include <cstring>
#include "../header/Cube.h"
#include "../../configs/defines.h"
#include "../gl/header/IndexBuffer.h"
#include "../gl/header/VertexBuffer.h"

Cube *Cube::instance = nullptr;

Cube *Cube::getInstance() {
    if (!instance)
        [[unlikely]]
                instance = new Cube();
    return instance;
}

Cube::Cube() {

}

unsigned Cube::addBlock(ChunkId chunkId, Block block, unsigned accumulator) {
    float margin = 0.0125f;
    unsigned count = 0;
    float displacementFactor = BlockType::getAlphaRatio(block.blockType);
    const BlockTextures *tmpBlockTexture = BlockType::getTextures(block.blockType);
    std::vector<int> faces;

    if (block.up) faces.push_back(UP);
    if (block.down) faces.push_back(DOWN);
    if (block.north) faces.push_back(NORTH);
    if (block.east) faces.push_back(EAST);
    if (block.west) faces.push_back(WEST);
    if (block.south) faces.push_back(SOUTH);
    if (BlockType::getBlockDef(block.blockType)->diagonal) {
        faces.push_back(DIAG1);
        faces.push_back(DIAG2);
    }

    for (int face: faces) {
        memcpy(temp, vertices[face], sizeof(temp));
        TextureID currentTexture;
        float displacement = 0.0f;
        if (face == UP)
            currentTexture = tmpBlockTexture->top;
        else if (face == DOWN)
            currentTexture = tmpBlockTexture->bottom;
        else {
            currentTexture = tmpBlockTexture->side;
            if (face != DIAG1 && face != DIAG2)
                displacement = displacementFactor;
        }

        for (Vertex &vertex: temp) {
            switch (face) {
                case NORTH:
                    vertex.position[2] += displacement;
                    break;
                case EAST:
                    vertex.position[0] -= displacement;
                    break;
                case WEST:
                    vertex.position[0] += displacement;
                    break;
                case SOUTH:
                    vertex.position[2] -= displacement;
                    break;
            }

            vertex.position[0] += (static_cast<GLfloat>(chunkId.x << 4) + static_cast<GLfloat>(block.x));
            vertex.position[1] += static_cast<GLfloat>(block.z);
            vertex.position[2] += ((-1.0f) * (static_cast<GLfloat>(chunkId.y << 4) + static_cast<GLfloat>(block.y)));

            vertex.texturePos[0] /= static_cast<float>(TEXTURES_IN_ROW);
            vertex.texturePos[1] /= static_cast<float>(TEXTURES_IN_ROW);
            if (vertex.texturePos[0] == 0.0) vertex.texturePos[0] += margin;
            else vertex.texturePos[0] -= margin;
            if (vertex.texturePos[1] == 0.0) vertex.texturePos[1] += margin;
            else vertex.texturePos[1] -= margin;

            vertex.texturePos[0] += currentTexture.x;
            vertex.texturePos[1] += currentTexture.y;
        }
        glBufferSubData(GL_ARRAY_BUFFER, (accumulator + count) * sizeof(temp), sizeof(temp), temp);
        count++;
    }
    return count;
}


Cube::~Cube() {

}