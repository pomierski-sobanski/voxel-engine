//
// Created by pawel on 06.07.2021.
//

#include <glm/vec3.hpp>
#include <glm/trigonometric.hpp>
#include <glm/geometric.hpp>
#include <glm/ext/matrix_transform.hpp>
#include "../header/Camera.h"
#include "../header/JsonConfig.h"

Camera *Camera::camera = 0;

Camera::PositionInfo   Camera::positionInfo = {glm::vec3(753.5f, 55.5f, -791.5f),
                                               glm::vec3(0.5f, 0.5f, -0.5f), {0, 0}, {0, 0}};

glm::vec3   Camera::prevCameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3   Camera::cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3   Camera::cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
bool        Camera::firstMouse = true;
float       Camera::yaw = -90.0f;
float       Camera::pitch = 0.0f;
float       Camera::lastX = 300;
float       Camera::lastY = 300;
float       Camera::cameraSpeed = 9.0f;
float       Camera::deltaTime = 0.0f;
float       Camera::previousTime = 0.0f;
float       Camera::sensitivity = 0.4f;

Camera::Camera(GLFWwindow *window) : window(window) {
    update();
}

Camera *Camera::getInstance(GLFWwindow *window) {
    if (!camera)
        [[unlikely]]
                camera = new Camera(window);
    return camera;
}

void Camera::mouse_callback(GLFWwindow *window, double xpos, double ypos) {
    if (firstMouse) {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos;
    lastX = xpos;
    lastY = ypos;

    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw += xoffset;  //  left/right
    pitch += yoffset; //  up/down

    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 front;
    front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(front);
}

void Camera::walk(unsigned int direction) {
    float coeff = cameraSpeed * deltaTime;
    try {
        switch (direction) {
            case FORWARD:
                positionInfo.cameraPos += coeff * cameraFront;
                break;
            case BACKWARD:
                positionInfo.cameraPos -= coeff * cameraFront;
                break;
            case RIGHT:
                positionInfo.cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * coeff;
                break;
            case LEFT:
                positionInfo.cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * coeff;
                break;
            case UP:
                positionInfo.cameraPos += cameraUp * coeff;
                break;
            case DOWN:
                positionInfo.cameraPos -= cameraUp * coeff;
                break;
            default:
                throw std::invalid_argument("direction(" + std::to_string(direction) + ") ");
        }
        if (PRINT_POSITION_INFO) {
            std::cout << "x:" << positionInfo.cameraPos[0]
                      << " y:" << positionInfo.cameraPos[1]
                      << " z:" << positionInfo.cameraPos[2]
                      << "\tChunkID: " << getChunkID().x << " " << getChunkID().y
                      << std::endl;
        }
    }
    catch (const std::invalid_argument &e) {
        std::cerr << "Invalid argument: " << e.what() << std::endl;
    }
}


glm::mat4 Camera::update() {
    updatePosition();
    updateChunkId();
    updateLookDir();
    updateTime(glfwGetTime());

    return glm::lookAt(positionInfo.cameraPos, positionInfo.cameraPos + cameraFront, cameraUp);
}


void Camera::updateLookDir() { prevCameraFront = cameraFront; };

void Camera::updatePosition() const { positionInfo.prevCameraPos = positionInfo.cameraPos; }

void Camera::updateTime(double time) const {
    deltaTime = (float) time - previousTime;
    previousTime = (float) time;
}

glm::vec3 Camera::getDirection() {
    glm::vec3 direction = {
            cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
            sin(glm::radians(pitch)),
            sin(glm::radians(yaw)) * cos(glm::radians(pitch))
    };
    return direction;
}

glm::vec3 &Camera::getPos() {
    return positionInfo.cameraPos;
}

glm::vec3 &Camera::getFront() {
    return cameraFront;
}

glm::vec3 &Camera::getUp() {
    return cameraUp;
}

Camera::~Camera() {

}

void Camera::updateChunkId() {
    positionInfo.prevChunkId.x = positionInfo.chunkId.x;
    positionInfo.prevChunkId.y = positionInfo.chunkId.y;

    positionInfo.chunkId.x = static_cast<uint32_t>(static_cast<int32_t>(positionInfo.cameraPos[0]) / CHUNK_SIZE);
    positionInfo.chunkId.y = static_cast<uint32_t>(abs(static_cast<int32_t>(positionInfo.cameraPos[2])) / CHUNK_SIZE);
}

ChunkId Camera::getChunkID() {
    return positionInfo.chunkId;
}

ChunkId Camera::getPrevChunkID() {
    return positionInfo.prevChunkId;
}

bool Camera::chunkChanged() {
    return !(positionInfo.chunkId == positionInfo.prevChunkId);
}

bool Camera::positionChanged() {
    return !(positionInfo.cameraPos == positionInfo.prevCameraPos);
}

bool Camera::lookDirChanged() {
    return !(prevCameraFront == cameraFront);
}