//
// Created by pawel on 05.09.2021.
//

#include "../header/ChunksData.h"
#include "../header/JsonConfig.h"
#include "../../configs/defines.h"
#include "../header/Cube.h"
#include "../gl/header/Shader.h"
#include "../header/Chunk.h"

ChunksData::ChunksData() {
    chunks = new Chunk *[visibleChunks()];

    for (int i = 0; i < visibleChunks(); ++i) {
        chunks[i] = new Chunk();
    }

    prepareIndexBuffer();

    prepareShaderProgram();

    for (int i = 0; i < visibleChunks(); ++i) {
        chunks[i]->setIb(ib);
        chunks[i]->setSp(&shaderProgram);
    }
}

void ChunksData::prepareShaderProgram() const {
    Shader vertexShader(VERTEX_SHADER_PATH, GL_VERTEX_SHADER);
    Shader fragmentShader(FRAGMENT_SHADER_PATH, GL_FRAGMENT_SHADER);

    shaderProgram.attach(vertexShader);
    shaderProgram.attach(fragmentShader);

    shaderProgram.link();

    vertexShader.deleteShader();
    fragmentShader.deleteShader();
}

void ChunksData::prepareIndexBuffer() {
    indices = new GLuint[INDEX_BUFFER_ELEMENTS];

    int max_count = INDEX_BUFFER_ELEMENTS;

    int offset = 0;
    for (int i = 0; i < max_count; i += 6) {
        indices[i] = Cube::indices[0] + offset;
        indices[i + 1] = Cube::indices[1] + offset;
        indices[i + 2] = Cube::indices[2] + offset;
        indices[i + 3] = Cube::indices[3] + offset;
        indices[i + 4] = Cube::indices[4] + offset;
        indices[i + 5] = Cube::indices[5] + offset;
        offset += 4;
    }

    ib = new IndexBuffer(indices, INDEX_BUFFER_ELEMENTS);
}

ChunksData::~ChunksData() {
    delete ib;

    delete[] indices;

    for (int i = 0; i < visibleChunks(); i++) delete chunks[i];

    delete[] chunks;

    shaderProgram.deleteProgram();
}

void ChunksData::resize(int size) {
    throw "Not Implemented";
}

void ChunksData::addChunk(ChunkId chunkId) {
    for (int i = 0; i < visibleChunks(); ++i) {
        if (!chunks[i]->isUsed()) {
            chunkMap[chunkId] = i;
            chunks[i]->setChunkId(chunkId);
            chunks[i]->use();
            return;
        }
    }
    assert(false);
}


void ChunksData::freeChunk(ChunkId chunkId) {
    chunks[chunkMap.at(chunkId)]->freeChunk();
}

void ChunksData::printData() {
    for (int i = 0; i < visibleChunks(); i++) {
        std::cout << "Chunk_" << i << " Used: " << chunks[i]->isUsed() << "  |  ";
    }
    std::cout << std::endl;
}

void ChunksData::loadNew(PerlinChunk *perlinChunk) {
    int chunkIndex = chunkMap[perlinChunk->getChunkId()];
    chunks[chunkIndex]->loadNew(perlinChunk);
}

void ChunksData::checkForReload() {
    for (int i = 0; i < visibleChunks(); ++i) {
        if (chunks[i]->isUsed() && chunks[i]->reloadRequired()) {

            chunks[i]->load();
            chunks[i]->perlinChunk->setChangedFlag(false);
        }
    }
}

void ChunksData::organizeInOrder() {
    if (visibleChunks() > 1) {
        std::sort(chunks, chunks + sizeof(Chunk *), [](Chunk *a, Chunk *b) {
            return (Camera::getInstance()->getChunkID() - a->getChunkId()).length() <
                   (Camera::getInstance()->getChunkID() - b->getChunkId()).length();
        });
    }
    for (int i = 0; i < visibleChunks(); ++i) {
        ChunkId id = chunks[i]->getChunkId();
        chunkMap[id] = i;
    }
}
