//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/biomes/TaigaBiome.h"

TaigaBiome::TaigaBiome() : Biome(Biome::TAIGA, TAIGA_MIN, TAIGA_MAX, Biome::TREES, {
        BlockType::GRASS_BLOCK,
        BlockType::DIRT,
        BlockType::STONE,
        BlockType::STONE,
        BlockType::BEDROCK
}) {}
