//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/biomes/PlainsBiome.h"

PlainsBiome::PlainsBiome() : Biome(Biome::PLAINS, PLAINS_MIN, PLAINS_MAX, Biome::GRASSES, {
        BlockType::GRASS_BLOCK,
        BlockType::DIRT,
        BlockType::STONE,
        BlockType::STONE,
        BlockType::BEDROCK
}) {}
