//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/biomes/WaterBiome.h"

WaterBiome::WaterBiome() : Biome(Biome::WATER, NOT_APPLICABLE, NOT_APPLICABLE, Biome::NO_PLANTS, {
        BlockType::SAND,
        BlockType::SAND,
        BlockType::STONE,
        BlockType::STONE,
        BlockType::BEDROCK
}) {}

void WaterBiome::calcFaces(BiomeFragment *frag) {
    uint32_t z = BEACH_MIN_Z;
    Faces faces;
    faces.type = BlockType::getBlockType(BlockType::WATER);
    faces.up = true;
    faces.down = false;
    faces.north = false;
    faces.west = false;
    faces.south = false;
    faces.east = false;
    addBlock(frag, &z, &faces);
    Biome::calcFaces(frag);
}
