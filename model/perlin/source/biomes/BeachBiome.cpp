//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/biomes/BeachBiome.h"

BeachBiome::BeachBiome() : Biome(Biome::BEACH, NOT_APPLICABLE, NOT_APPLICABLE, Biome::NO_PLANTS, {
        BlockType::SAND,
        BlockType::SAND,
        BlockType::STONE,
        BlockType::STONE,
        BlockType::BEDROCK
}) {}
