//
// Created by pawel on 05.12.2021.
//

#include "../../header/events/PreviousBlockButtonClickEvent.h"

void PreviousBlockButtonClickEvent::handle(Perlin *perlin) {
    perlin->decBlockTypeToPlaceIndex();
}
