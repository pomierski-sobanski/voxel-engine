//
// Created by ms on 03.10.2021.
//

#include "../../header/events/MouseButtonClickEvent.h"
#include <cmath>

#define MOD_X(x) ((x) % CHUNK_SIZE)
#define MOD_Y(y) ((y) % CHUNK_SIZE)
#define MOD_Z(z) (std::min((z), static_cast<uint32_t>(CHUNK_HEIGHT - 1)))

void MouseButtonClickEvent::handle(Perlin *perlin) {
    const float *cameraPoint = perlin->getCameraPoint();
    const float *cameraVector = perlin->getCameraVector();

    const float radius = GRASP_RADIUS + 1.0f;

    const float minX = static_cast<uint32_t>(
            std::max(0.0f, cameraPoint[0] - radius));
    const float minY = static_cast<uint32_t>(
            std::max(0.0f, cameraPoint[1] - radius));
    const float minZ = static_cast<uint32_t>(
            std::max(0.0f, cameraPoint[2] - radius));

    const float maxX = static_cast<uint32_t>(
            std::min(static_cast<float>(UINT32_MAX), cameraPoint[0] + radius));
    const float maxY = static_cast<uint32_t>(
            std::min(static_cast<float>(UINT32_MAX), cameraPoint[1] + radius));
    const float maxZ = static_cast<uint32_t>(
            std::min(static_cast<float>(UINT8_MAX), cameraPoint[2] + radius));

    FaceInfo faceInfo = {radius + 1.0f, UINT8_MAX, 0, 0, 0};
    for (uint32_t x = minX; x <= maxX; ++x) {
        const float sqX = sq(cameraPoint[0] - x);
        for (uint32_t y = minY; y <= maxY; ++y) {
            const float sqY = sq(cameraPoint[1] - y);
            PerlinChunk *chunk = getChunk(perlin, x, y);
            for (uint32_t z = minZ; z <= maxZ; ++z) {
                const float sqZ = sq(cameraPoint[2] - z);
                if (std::sqrt(sqX + sqY + sqZ) > radius) continue;
                Block *block = getBlock(chunk, x, y, z);
                if (block == nullptr) continue;
                const float blockXYZ[3] = {static_cast<float>(x),
                                           static_cast<float>(y),
                                           static_cast<float>(z)};
                std::pair<float, faceType> nearest =
                        RayPlaneIntersection::nearestIntersectedFace(
                                block, blockXYZ, cameraPoint, cameraVector);
                if (nearest.second != UINT8_MAX &&
                    nearest.first < faceInfo.distance) {
                    faceInfo.distance = nearest.first;
                    faceInfo.fType = nearest.second;
                    faceInfo.x = x;
                    faceInfo.y = y;
                    faceInfo.z = z;
                }
            }
        }
    }

    if (faceInfo.distance > radius) return;

    if (isPlaceBlockEvent()) {
        if (faceInfo.fType == DOWN) faceInfo.z = DOWN_BLOCK(faceInfo.z);
        else if (faceInfo.fType == UP) faceInfo.z = UP_BLOCK(faceInfo.z);
        else if (faceInfo.fType == SOUTH) faceInfo.y = SOUTH_BLOCK(faceInfo.y);
        else if (faceInfo.fType == NORTH) faceInfo.y = NORTH_BLOCK(faceInfo.y);
        else if (faceInfo.fType == WEST) faceInfo.x = WEST_BLOCK(faceInfo.x);
        else if (faceInfo.fType == EAST) faceInfo.x = EAST_BLOCK(faceInfo.x);
        else return;
    }

    Block blockToPlace;
    NearestBlocks nearestBlocks;
    nearestBlocks.centralBlock = getBlockPair(perlin, faceInfo.x, faceInfo.y, faceInfo.z);
    if (isPlaceBlockEvent()) {
        if (std::get<0>(nearestBlocks.centralBlock) != nullptr) return;
        else {
            blockToPlace.x = CHUNK_X_BITMASK(faceInfo.x % CHUNK_SIZE);
            blockToPlace.y = CHUNK_Y_BITMASK(faceInfo.y % CHUNK_SIZE);
            blockToPlace.z = CHUNK_Z_BITMASK(std::min(faceInfo.z, static_cast<uint32_t>(CHUNK_HEIGHT - 1)));
            std::get<0>(nearestBlocks.centralBlock) = &blockToPlace;
            std::get<1>(nearestBlocks.centralBlock) = getChunk(perlin, faceInfo.x, faceInfo.y);
        }
    } else {
        if (std::get<0>(nearestBlocks.centralBlock) == nullptr) return;
    }

    if (DOWN_BLOCK(faceInfo.z) >= minZ) {
        nearestBlocks.downBlock = getBlockPair(perlin, faceInfo.x, faceInfo.y, DOWN_BLOCK(faceInfo.z));
    }
    if (UP_BLOCK(faceInfo.z) <= maxZ) {
        nearestBlocks.upBlock = getBlockPair(perlin, faceInfo.x, faceInfo.y, UP_BLOCK(faceInfo.z));
    }
    if (SOUTH_BLOCK(faceInfo.y) >= minY) {
        nearestBlocks.southBlock = getBlockPair(perlin, faceInfo.x, SOUTH_BLOCK(faceInfo.y), faceInfo.z);
    }
    if (NORTH_BLOCK(faceInfo.y) <= maxY) {
        nearestBlocks.northBlock = getBlockPair(perlin, faceInfo.x, NORTH_BLOCK(faceInfo.y), faceInfo.z);
    }
    if (WEST_BLOCK(faceInfo.x) >= minX) {
        nearestBlocks.westBlock = getBlockPair(perlin, WEST_BLOCK(faceInfo.x), faceInfo.y, faceInfo.z);
    }
    if (EAST_BLOCK(faceInfo.x) <= maxX) {
        nearestBlocks.eastBlock = getBlockPair(perlin, EAST_BLOCK(faceInfo.x), faceInfo.y, faceInfo.z);
    }
    modifyBlocks(perlin, nearestBlocks);
}

PerlinChunk *MouseButtonClickEvent::getChunk(Perlin *perlin, uint32_t x, uint32_t y) {
    return perlin->generateChunk({x / CHUNK_SIZE, y / CHUNK_SIZE});
}

Block *MouseButtonClickEvent::getBlock(PerlinChunk *chunk,
                                       uint32_t x, uint32_t y, uint32_t z) {
    const uint32_t _x = MOD_X(x);
    const uint32_t _y = MOD_Y(y);
    const uint32_t _z = MOD_Z(z);
    return chunk->getBlock(chunk->getBlocksArr3D()[_x][_y][_z], _x, _y, _z);
}

BlockTuple MouseButtonClickEvent::getBlockPair(Perlin *perlin, uint32_t x,
                                               uint32_t y, uint32_t z) {
    PerlinChunk *chunk = getChunk(perlin, x, y);
    return {getBlock(chunk, x, y, z), chunk, MOD_X(x), MOD_Y(y), MOD_Z(z)};
}

uint8_t MouseButtonClickEvent::getBlockType(const BlockTuple *t) {
    uint8_t ***blocksArr3D = std::get<1>(*t)->getBlocksArr3D();
    return blocksArr3D[std::get<2>(*t)][std::get<3>(*t)][std::get<4>(*t)];
}

Block MouseButtonClickEvent::getNewBlock(const BlockTuple *t) {
    Block b;
    b.x = std::get<2>(*t);
    b.y = std::get<3>(*t);
    b.z = std::get<4>(*t);
    b.up = 0x0;
    b.down = 0x0;
    b.north = 0x0;
    b.west = 0x0;
    b.south = 0x0;
    b.east = 0x0;
    b.blockType = 0x0;
    return b;
}
