//
// Created by Michał Sobański on 03.10.2021.
//

#include "../../header/events/RightMouseButtonClickEvent.h"

bool RightMouseButtonClickEvent::isPlaceBlockEvent() {
    return true;
}

void RightMouseButtonClickEvent::modifyBlocks(Perlin *perlin,
                                              const MouseButtonClickEvent::NearestBlocks &nearestBlocks) {
    Block *b = std::get<0>(nearestBlocks.centralBlock);
    b->up = 0x1;
    b->down = 0x1;
    b->north = 0x1;
    b->west = 0x1;
    b->south = 0x1;
    b->east = 0x1;
    b->blockType = (perlin->getBlockTypeToPlace()) & 0xff;
    std::get<1>(nearestBlocks.centralBlock)->placeBlock(*b);
}
