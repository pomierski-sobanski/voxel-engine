//
// Created by Michał Sobański on 03.10.2021.
//

#include "../../header/events/LeftMouseButtonClickEvent.h"

#define CAN_BE_BROKEN(t) (BlockType::getBlockType(std::get<0>(t)->blockType)->canBeBroken())

bool LeftMouseButtonClickEvent::isPlaceBlockEvent() {
    return false;
}

void LeftMouseButtonClickEvent::modifyBlocks(Perlin *perlin,
                                             const MouseButtonClickEvent::NearestBlocks &nearestBlocks) {
    if (!CAN_BE_BROKEN(nearestBlocks.centralBlock)) return;

    if (std::get<1>(nearestBlocks.upBlock) != nullptr) {
        if (std::get<0>(nearestBlocks.upBlock) != nullptr) {
            std::get<0>(nearestBlocks.upBlock)->down = 0x1;
        } else if (getBlockType(&nearestBlocks.upBlock) != 0) {
            Block b = getNewBlock(&nearestBlocks.upBlock);
            b.down = 0x1;
            std::get<1>(nearestBlocks.upBlock)->addNewBlock(b);
        }
    }

    if (std::get<1>(nearestBlocks.downBlock) != nullptr) {
        if (std::get<0>(nearestBlocks.downBlock) != nullptr) {
            std::get<0>(nearestBlocks.downBlock)->up = 0x1;
        } else if (getBlockType(&nearestBlocks.downBlock) != 0) {
            Block b = getNewBlock(&nearestBlocks.downBlock);
            b.up = 0x1;
            std::get<1>(nearestBlocks.downBlock)->addNewBlock(b);
        }
    }

    if (std::get<1>(nearestBlocks.northBlock) != nullptr) {
        if (std::get<0>(nearestBlocks.northBlock) != nullptr) {
            std::get<0>(nearestBlocks.northBlock)->south = 0x1;
        } else if (getBlockType(&nearestBlocks.northBlock) != 0) {
            Block b = getNewBlock(&nearestBlocks.northBlock);
            b.south = 0x1;
            std::get<1>(nearestBlocks.northBlock)->addNewBlock(b);
        }
    }

    if (std::get<1>(nearestBlocks.southBlock) != nullptr) {
        if (std::get<0>(nearestBlocks.southBlock) != nullptr) {
            std::get<0>(nearestBlocks.southBlock)->north = 0x1;
        } else if (getBlockType(&nearestBlocks.southBlock) != 0) {
            Block b = getNewBlock(&nearestBlocks.southBlock);
            b.north = 0x1;
            std::get<1>(nearestBlocks.southBlock)->addNewBlock(b);
        }
    }

    if (std::get<1>(nearestBlocks.eastBlock) != nullptr) {
        if (std::get<1>(nearestBlocks.eastBlock) != nullptr) {
            if (std::get<0>(nearestBlocks.eastBlock) != nullptr) {
                std::get<0>(nearestBlocks.eastBlock)->west = 0x1;
            } else if (getBlockType(&nearestBlocks.eastBlock) != 0) {
                Block b = getNewBlock(&nearestBlocks.eastBlock);
                b.west = 0x1;
                std::get<1>(nearestBlocks.eastBlock)->addNewBlock(b);
            }
        }
    }

    if (std::get<1>(nearestBlocks.westBlock) != nullptr) {
        if (std::get<0>(nearestBlocks.westBlock) != nullptr) {
            std::get<0>(nearestBlocks.westBlock)->east = 0x1;
        } else if (getBlockType(&nearestBlocks.westBlock) != 0) {
            Block b = getNewBlock(&nearestBlocks.westBlock);
            b.east = 0x1;
            std::get<1>(nearestBlocks.westBlock)->addNewBlock(b);
        }
    }

    std::get<1>(nearestBlocks.centralBlock)->breakBlock(
            std::get<0>(nearestBlocks.centralBlock));
}
