//
// Created by Michał Sobański on 06.08.2021.
//

#include "../header/PerlinPlants.h"
#include "../header/PerlinChunk.h"
#include "../header/BlockType.h"
#include "../header/Biome.h"

PerlinChunk::PerlinChunk(const ChunkId &id, const uint32_t &worldSeed)
        : id(id), cornerX(id.x * CHUNK_SIZE), cornerY(id.y * CHUNK_SIZE),
          x0(cornerX + 1), y0(cornerY + 1), worldSeed(uint64_t(worldSeed)) {
    changed.store(false);
    facesSum = new std::array<uint32_t, BlockType::Id::NUM_OF_BLOCK_TYPES>;
    blocks = new std::array<std::vector<Block> *, BlockType::Id::NUM_OF_BLOCK_TYPES>;
    for (int i = 0; i < BlockType::Id::NUM_OF_BLOCK_TYPES; ++i) {
        facesSum->at(i) = 0;
        blocks->at(i) = new std::vector<Block>;
    }
    elevation = new uint32_t *[ELEVATION_ARR_SIZE];
    biome = new uint8_t *[ELEVATION_ARR_SIZE];
    for (uint32_t i = 0; i < ELEVATION_ARR_SIZE; ++i) {
        elevation[i] = new uint32_t[ELEVATION_ARR_SIZE];
        biome[i] = new uint8_t[ELEVATION_ARR_SIZE];
    }
    blocksArr3D = new uint8_t **[CHUNK_SIZE];
    for (int i = 0; i < CHUNK_SIZE; ++i) {
        blocksArr3D[i] = new uint8_t *[CHUNK_SIZE];
        for (int j = 0; j < CHUNK_SIZE; ++j) {
            blocksArr3D[i][j] = new uint8_t[CHUNK_HEIGHT];
            for (int k = 0; k < CHUNK_HEIGHT; ++k) {
                blocksArr3D[i][j][k] = 0;
            }
        }
    }
}

PerlinChunk::~PerlinChunk() {
    for (auto it = blocks->begin(); it != blocks->end(); ++it) {
        delete *it;
    }
    delete blocks;
    delete facesSum;
    for (uint32_t i = 0; i < ELEVATION_ARR_SIZE; ++i) {
        delete[] elevation[i];
        delete[] biome[i];
    }
    delete[] elevation;
    delete[] biome;

    for (int i = 0; i < CHUNK_SIZE; ++i) {
        for (int j = 0; j < CHUNK_SIZE; ++j) {
            delete[] blocksArr3D[i][j];
        }
        delete[] blocksArr3D[i];
    }
    delete[] blocksArr3D;
}

const std::array<std::vector<Block> *, BlockType::Id::NUM_OF_BLOCK_TYPES> *
PerlinChunk::getBlocks() const {
    return blocks;
}

void PerlinChunk::setElevation(uint8_t x, uint8_t y, uint32_t z) {
    elevation[x][y] = z;
}

void PerlinChunk::setBiome(uint8_t x, uint8_t y, uint8_t biomeId) {
    biome[x][y] = biomeId;
}

Block *PerlinChunk::getBlock(uint8_t blockType, uint32_t x,
                             uint32_t y, uint32_t z) const {
    std::vector<block> *vec = blocks->at(blockType);
    for (auto it = vec->begin(); it != vec->end(); ++it) {
        if ((it->x == x) && (it->y == y) && (it->z == z)) {
            return &*it;
        }
    }
    return nullptr;
}

void PerlinChunk::placeBlock(Block block) {
    std::vector<Block> *vec = blocks->at(block.blockType);
    vec->push_back(block);
    blocksArr3D[block.x][block.y][block.z] = block.blockType;
    changed.store(true);
}

void PerlinChunk::breakBlock(Block *block) {
    if (block->blockType >= BlockType::Id::NUM_OF_BLOCK_TYPES) return;
    std::vector<Block> *vec = blocks->at(block->blockType);
    blocksArr3D[block->x][block->y][block->z] = 0;
    for (auto it = vec->begin(); it != vec->end(); ++it) {
        if ((it->x == block->x) && (it->y == block->y) && (it->z == block->z)) {
            std::cout << "PerlinChunk::breakBlock(" << uint64_t(block->x) << ","
                      << uint64_t(block->y) << "," << uint64_t(block->z) << ")" << std::endl;
            vec->erase(it);
            changed.store(true);
            return;
        }
    }
}

void PerlinChunk::addNewBlock(Block block) {
    const uint8_t blockType = blocksArr3D[block.x][block.y][block.z];
    block.blockType = blockType;
    blocks->at(blockType)->push_back(block);
    changed.store(true);
}

uint8_t ***PerlinChunk::getBlocksArr3D() const {
    return blocksArr3D;
}

const std::atomic<bool> &PerlinChunk::getChangedFlag() const {
    return changed;
}

void PerlinChunk::setChangedFlag(const bool changed) {
    this->changed.store(changed);
}

void PerlinChunk::calcFaces() {
    for (uint8_t x = 1; x <= CHUNK_SIZE; x++) {
        for (uint8_t y = 1; y <= CHUNK_SIZE; y++) {
            // W ===(x)===> E
            // S ===(y)===> N
            BiomeFragment frag;
            frag.x = &x;
            frag.y = &y;
            frag.blocks = this->blocks;
            frag.centralB = Biome::getBiome(biome[x][y]);
            frag.eB = Biome::getBiome(biome[EAST_BLOCK(x)][y]);
            frag.nB = Biome::getBiome(biome[x][NORTH_BLOCK(y)]);
            frag.wB = Biome::getBiome(biome[WEST_BLOCK(x)][y]);
            frag.sB = Biome::getBiome(biome[x][SOUTH_BLOCK(y)]);
            frag.eZ = &elevation[EAST_BLOCK(x)][y];
            frag.nZ = &elevation[x][NORTH_BLOCK(y)];
            frag.wZ = &elevation[WEST_BLOCK(x)][y];
            frag.sZ = &elevation[x][SOUTH_BLOCK(y)];
            frag.centralZ = &elevation[x][y];
            frag.maxZ = &elevation[x][y];
            uint32_t minZ = std::min(
                    {*frag.sZ, *frag.wZ, *frag.nZ, *frag.eZ, *frag.centralZ});
            frag.minZ = &minZ;
            frag.centralB->calcFaces(&frag);
        }
    }
}

void PerlinChunk::sumFaces() {
    for (int i = 0; i < BlockType::Id::NUM_OF_BLOCK_TYPES; ++i) {
        std::vector<Block> *vector = blocks->at(i);
        uint32_t sum = 0;
        for (auto it = vector->begin(); it != vector->end(); ++it) {
            sum += (it->up);
            sum += (it->down);
            sum += (it->north);
            sum += (it->west);
            sum += (it->south);
            sum += (it->east);
        }
        facesSum->at(i) += sum;
        numOfFaces += sum;
    }
}

void PerlinChunk::addPlants() {
    PerlinPlants(&id, &this->worldSeed, this->biome,
                 this->elevation, blocks).generatePlants();
}

void PerlinChunk::setBlocksArr3D() {
    for (auto it = blocks->begin(); it != blocks->end(); ++it) {
        for (auto it2 = (*it)->begin(); it2 != (*it)->end(); ++it2) {
            Block &block = *it2;
            blocksArr3D[block.x][block.y][block.z] = block.blockType;
        }
    }
    for (uint32_t x = 0; x < CHUNK_SIZE; ++x) {
        for (uint32_t y = 0; y < CHUNK_SIZE; ++y) {
            const uint32_t elev = elevation[CHUNK_ARR_REAL_X(x)][CHUNK_ARR_REAL_Y(y)];
            for (uint32_t z = 0; (z <= elev); ++z) {
                if (blocksArr3D[x][y][z] == 0) {
                    Biome *b = Biome::getBiome(biome[CHUNK_ARR_REAL_X(x)][CHUNK_ARR_REAL_Y(y)]);
                    uint8_t blockType = b->getBlockType(z, elev)->getId();
                    if (blockType >= BlockType::Id::NUM_OF_BLOCK_TYPES) continue;
                    blocksArr3D[x][y][z] = blockType;
                }
            }
        }
    }
}

uint32_t PerlinChunk::getCornerX() const {
    return cornerX;
}

uint32_t PerlinChunk::getCornerY() const {
    return cornerY;
}

const ChunkId *PerlinChunk::getId() const {
    return &id;
}

uint32_t PerlinChunk::getBitPosition(uint32_t x, uint32_t y) {
    uint32_t pos = CHUNK_X_BITMASK(x) << CHUNK_Y_BITS;
    pos = pos | CHUNK_Y_BITMASK(y);
    return pos;
}

uint32_t PerlinChunk::getBitPosition(uint32_t x, uint32_t y, uint32_t z) {
    uint32_t pos = CHUNK_X_BITMASK(x) << CHUNK_Y_BITS;
    pos = (pos | CHUNK_Y_BITMASK(y)) << CHUNK_Z_BITS;
    pos = pos | CHUNK_Z_BITMASK(z);
    return pos;
}

void PerlinChunk::printElevation() {
    std::cout << "PerlinChunk elevation:" << std::endl;
    for (uint8_t x = 0; x < ELEVATION_ARR_SIZE; x++) {
        for (uint8_t y = 0; y < ELEVATION_ARR_SIZE; y++) {
            std::cout << elevation[x][y];
            if (elevation[x][y] >= 100) std::cout << " ";
            else if (elevation[x][y] >= 10) std::cout << "  ";
            else std::cout << "   ";
        }
        std::cout << std::endl;
    }
}

void PerlinChunk::printBiome() {
    std::cout << "PerlinChunk biome:" << std::endl;
    for (uint8_t x = 0; x < ELEVATION_ARR_SIZE; x++) {
        for (uint8_t y = 0; y < ELEVATION_ARR_SIZE; y++) {
            std::cout << ((uint32_t) biome[x][y]);
            if (biome[x][y] >= 100) std::cout << " ";
            else if (biome[x][y] >= 10) std::cout << "  ";
            else std::cout << "   ";
        }
        std::cout << std::endl;
    }
}

void PerlinChunk::printBlocks() {
    for (int i = 0; i < (*blocks).size(); ++i) {
        std::vector<Block> *vector = (*blocks)[i];
        for (auto j = 0; j < (*vector).size(); j++) {
            Block b = (*vector)[j];
            std::cout << "Block { ";
            int x = b.x & 0xff;
            int y = b.y & 0xff;
            int z = b.z & 0xff;
            std::cout << "x=" << x;
            std::cout << (x > 99 ? " " : (x > 9 ? "  " : "   "));
            std::cout << "y=" << y;
            std::cout << (y > 99 ? " " : (y > 9 ? "  " : "   "));
            std::cout << "z=" << z << "   ";
            if (b.up) std::cout << "U ";
            if (b.down) std::cout << "D ";
            if (b.north) std::cout << "N ";
            if (b.west) std::cout << "W ";
            if (b.south) std::cout << "S ";
            if (b.east) std::cout << "E ";
            std::cout << "}" << std::endl;
        }
    }
}


const std::array<uint32_t, BlockType::Id::NUM_OF_BLOCK_TYPES> *
PerlinChunk::getFacesSum() const {
    return facesSum;
}

uint32_t PerlinChunk::getNumOfFaces() const {
    return numOfFaces;
}

const ChunkId &PerlinChunk::getChunkId() const {
    return id;
}

bool PerlinChunk::isBelowSeaLevel(float z) {
    return ((float) SEA_LEVEL) > z;
}

