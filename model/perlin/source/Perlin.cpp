//
// Created by Michał Sobański on 06.08.2021.
//

#include "../../rng/header/SimpleRng.h"
#include "../header/Perlin.h"
#include "../header/BlockType.h"
#include "../../header/World.h"
#include "../../header/JsonConfig.h"
#include "../../header/Camera.h"
#include <cmath>
#include <random>
#include <numeric>

Perlin::Perlin(World *world) : savingInProgress(false), cameraPoint{0, 0, 0}, cameraVector{0, 0, 0} {
    this->world = world;
    perm.resize(PERM_VECTOR_SIZE);
    std::iota(perm.begin(), perm.end(), 0); // fill with values, range [0, 255]
    std::default_random_engine engine(world->getSeed()); // initialize random engine
    std::shuffle(perm.begin(), perm.end(), engine); // shuffle using random engine
    perm.insert(perm.end(), perm.begin(), perm.end()); // duplicate vector
    blockTypeToPlaceIndex = 0;
    chunks = new std::list<PerlinChunk *>;
    thread_event_handler = std::thread(&Perlin::handleEvents, this);
    thread_generator = std::thread(&Perlin::generateWorld, this);
}

Perlin::~Perlin() {
    for (PerlinChunk *c: *chunks) delete c;
    chunks->clear();
    delete chunks;
    thread_generator.join();
    thread_event_handler.join();
    if (savingInProgress.load()) {
        thread_saving.join();
    }
}

void Perlin::generateWorld() {
    while (world && world->running.load()) {
        ChunkId chunkId = world->perlinInput.dequeueAndWait();
        if (!(world && world->running.load())) return;
        PerlinChunk *chunk = generateChunk(chunkId);
        world->perlinOutput.enqueue(chunk);
    }
}

void Perlin::handleEvents() {
    while (world && world->running.load()) {
        PerlinEvent *event = world->eventQueue.dequeueAndWait();
        if (!(world && world->running.load())) return;
        event->handle(this);
        delete event;
    }
}

void Perlin::setCameraPoint(const glm::vec3 point) {
    for (int i = 0; i < 3; ++i) this->cameraPoint[i] = point[i];
}

void Perlin::setCameraVector(const glm::vec3 vector) {
    for (int i = 0; i < 3; ++i) this->cameraVector[i] = vector[i];
    if (PRINT_DIRECTION_INFO) printCameraDirection();
}

void Perlin::printCameraDirection() {
    static std::string lastDirection;
    int index = -1;
    for (int i = 0; i < 3; ++i) {
        if (std::abs(cameraVector[i]) > 0.8f) {
            const float n1 = std::abs(cameraVector[(i + 1) % 3]);
            const float n2 = std::abs(cameraVector[(i + 2) % 3]);
            if (n1 < 0.2f && n2 < 0.2f) {
                index = i;
                break;
            }
        }
    }
    if (index < 0) return;
    std::string direction = lastDirection;
    if (index == 0) {
        if (cameraVector[index] > 0.0f) direction = "EAST";
        else if (cameraVector[index] < 0.0f) direction = "WEST";
    } else if (index == 1) {
        if (cameraVector[index] > 0.0f) direction = "NORTH";
        else if (cameraVector[index] < 0.0f) direction = "SOUTH";
    } else if (index == 2) {
        if (cameraVector[index] > 0.0f) direction = "UP";
        else if (cameraVector[index] < 0.0f) direction = "DOWN";
    }
    if (direction == lastDirection) return;
    std::cout << (lastDirection = direction) << std::endl;
}

const float *Perlin::getCameraPoint() const {
    return cameraPoint;
}

const float *Perlin::getCameraVector() const {
    return cameraVector;
}

uint8_t Perlin::getBlockTypeToPlace() const {
    return PlaceableBlockTypes[blockTypeToPlaceIndex];
}

void Perlin::incBlockTypeToPlaceIndex() {
    blockTypeToPlaceIndex = (blockTypeToPlaceIndex + 1) % PLACEABLE_ARR_LEN;
    std::cout << "blockTypeToPlaceIndex: " << ((int) blockTypeToPlaceIndex) << std::endl;
    std::cout << "blockTypeToPlace: " << ((int) getBlockTypeToPlace()) << std::endl;
}

void Perlin::decBlockTypeToPlaceIndex() {
    if (blockTypeToPlaceIndex < 1) {
        blockTypeToPlaceIndex = PLACEABLE_ARR_LEN - 1;
    } else {
        blockTypeToPlaceIndex--;
    }
    std::cout << "blockTypeToPlaceIndex: " << ((int) blockTypeToPlaceIndex) << std::endl;
    std::cout << "blockTypeToPlace: " << ((int) getBlockTypeToPlace()) << std::endl;
}

void Perlin::printFacesStats(const uint32_t numOfChunks) {
    uint32_t generatedChunks = 0;
    double avgNumOfFaces = 0;
    double minNumOfFaces = INT32_MAX;
    double maxNumOfFaces = INT32_MIN;
    std::array<double, BlockType::Id::NUM_OF_BLOCK_TYPES> avg = {};
    std::array<double, BlockType::Id::NUM_OF_BLOCK_TYPES> min = {};
    std::array<double, BlockType::Id::NUM_OF_BLOCK_TYPES> max = {};
    for (int i = 0; i < BlockType::Id::NUM_OF_BLOCK_TYPES; ++i) {
        avg.at(i) = 0;
        min.at(i) = INT32_MAX;
        max.at(i) = INT32_MIN;
    }
    uint64_t rngSeed = time(nullptr);
    SimpleRng rng = SimpleRng(rngSeed);
    std::cout << std::endl << "Progress: 0/" << numOfChunks << std::endl;
    for (int ch = 0; ch < numOfChunks; ++ch) {
        uint32_t x = rng.randInt(INT32_MIN, INT32_MAX);
        uint32_t y = rng.randInt(INT32_MIN, INT32_MAX);
        PerlinChunk *chunk = generateChunk({x, y});
        avgNumOfFaces += static_cast<double>(chunk->getNumOfFaces());
        minNumOfFaces = std::min(minNumOfFaces, static_cast<double>(chunk->getNumOfFaces()));
        maxNumOfFaces = std::max(maxNumOfFaces, static_cast<double>(chunk->getNumOfFaces()));
        for (int i = 0; i < BlockType::Id::NUM_OF_BLOCK_TYPES; ++i) {
            const double sum = static_cast<double>(chunk->getFacesSum()->at(i));
            avg.at(i) += sum;
            min.at(i) = std::min(min.at(i), sum);
            max.at(i) = std::max(max.at(i), sum);
        }
        generatedChunks++;
        std::cout << "Progress: " << generatedChunks << "/" << numOfChunks << std::endl;
    }
    avgNumOfFaces = avgNumOfFaces / numOfChunks;
    for (int i = 0; i < BlockType::Id::NUM_OF_BLOCK_TYPES; ++i) {
        avg.at(i) = avg.at(i) / numOfChunks;
    }
    std::cout << std::endl;
    std::cout << "generatedChunks     = " << generatedChunks << std::endl;
    std::cout << "avgNumOfFaces/chunk = " << avgNumOfFaces << std::endl;
    std::cout << "minNumOfFaces/chunk = " << minNumOfFaces << std::endl;
    std::cout << "maxNumOfFaces/chunk = " << maxNumOfFaces << std::endl;
    for (int i = 0; i < BlockType::Id::NUM_OF_BLOCK_TYPES; ++i) {
        std::cout << "BlockType::Id::" << i << " => (avg|min|max) => ";
        std::cout << avg.at(i) << " | ";
        std::cout << min.at(i) << " | ";
        std::cout << max.at(i) << std::endl;
    }
    std::cout << std::endl;
}

void Perlin::cacheData(PerlinChunk *newChunk) {
    chunks->push_front(newChunk);
    if (chunks->size() > PERLIN_CACHE_SIZE) {
        PerlinChunk *oldChunk = chunks->back();
        delete oldChunk;
        chunks->pop_back();
    }
}

PerlinChunk *Perlin::getCachedData(const ChunkId &id) {
    for (auto it = chunks->cbegin(); it != chunks->cend(); ++it) {
        PerlinChunk *chunk = *it;
        if (*(chunk->getId()) == id) {
            chunks->erase(it);
            chunks->push_front(chunk);
            return chunk;
        }
    }
    return nullptr;
}

PerlinChunk *Perlin::generateChunk(const ChunkId &id) {
    PerlinChunk *chunk = getCachedData(id);
    if (chunk != nullptr) return chunk;
    chunk = new PerlinChunk(id, world->getSeed());
    for (uint8_t x = 0; x < ELEVATION_ARR_SIZE; x++) {
        for (uint8_t y = 0; y < ELEVATION_ARR_SIZE; y++) {
            uint32_t coordX = chunk->getCornerX() + x;
            uint32_t coordY = chunk->getCornerY() + y;
            uint32_t coordZ = generateZ(coordX, coordY);
            chunk->setElevation(x, y, coordZ);
            chunk->setBiome(x, y, generateBiome(coordX, coordY, coordZ));
        }
    }
    chunk->calcFaces();
    chunk->addPlants();
    chunk->sumFaces();
    chunk->setBlocksArr3D();
    cacheData(chunk);
    return chunk;
}

uint32_t Perlin::generateZ(uint32_t x, uint32_t y) {
    double z = 0.0;
    generateBase((double) x, (double) y, &z);
    generateLandform((double) x, (double) y, &z);
    return (uint32_t) z;
}

Biome::Id Perlin::generateBiome(uint32_t x, uint32_t y, uint32_t z) {
    double coeff = 0.0;
    for (uint32_t i = 0; i < BIOME_NUM_OF_COEFF; i++) {
        x += BIOME_INPUT_TRANSLATION;
        y += BIOME_INPUT_TRANSLATION;
        coeff += coeff2D(x, y, BIOME_INPUT_MODIF);
    }
    return Biome::getBiomeId(z, coeff / BIOME_NUM_OF_COEFF);
}

double Perlin::coeff2D(double x, double y, double typeModif) {
    return noise(typeModif * SCALE_INPUT_MODIF * x, typeModif * SCALE_INPUT_MODIF * y, 0.0);
}

void Perlin::addDiffFreq2D(double x, double y, double *z) {
    double modif_in = 1.00;
    double modif_out = 1.00;
    double modif_out_sum = 0.0;
    for (int32_t i = 0; i < 5; i += 1) {
        //noise - different frequencies
        *z += (modif_out * coeff2D(x, y, modif_in));
        modif_out_sum += modif_out;
        modif_in = modif_in * 2.0;
        modif_out = modif_out / 2.0;
    }
    *z /= modif_out_sum;
}

void Perlin::rescaleZ(double *z) {
    *z = *z * SCALE_Z_MODIF;
}

void Perlin::powerNoise2D(double x, double y, double *z, double summand,
                          double multiplicand, double powerModif) {
    double exponent = summand + (multiplicand * coeff2D(x, y, powerModif));
    *z = std::pow(*z, exponent);
}

void Perlin::generateBase(double x, double y, double *z) {
    addDiffFreq2D(x, y, z);
    powerNoise2D(x, y, z, BASE_POWER_SUMMAND, BASE_POWER_MULTIPLICAND, BASE_POWER_MODIF);
    rescaleZ(z);
}

void Perlin::generateLandform(double x, double y, double *z) {
    *z += (LAND_OUTPUT_MODIF * coeff2D(x, y, LAND_INPUT_MODIF));
    powerNoise2D(x, y, z, LAND_POWER_SUMMAND, LAND_POWER_MULTIPLICAND, LAND_POWER_MODIF);
}

double Perlin::noise(double x, double y, double z) {
    // Find the unit cube that contains the point
    int32_t X = (int32_t) floor(x) & 255;
    int32_t Y = (int32_t) floor(y) & 255;
    int32_t Z = (int32_t) floor(z) & 255;

    // Find relative x, y, z of point in cube
    x -= floor(x);
    y -= floor(y);
    z -= floor(z);

    // Compute fade curves for each of x, y, z
    double u = fade(x);
    double v = fade(y);
    double w = fade(z);

    // Hash coordinates of the 8 cube corners
    int32_t A = perm[X] + Y;
    int32_t AA = perm[A] + Z;
    int32_t AB = perm[A + 1] + Z;
    int32_t B = perm[X + 1] + Y;
    int32_t BA = perm[B] + Z;
    int32_t BB = perm[B + 1] + Z;

    // Add blended results from 8 corners of cube
    double res = lerp(w, lerp(v, lerp(u, grad(perm[AA], x, y, z), grad(perm[BA], x - 1, y, z)),
                              lerp(u, grad(perm[AB], x, y - 1, z), grad(perm[BB], x - 1, y - 1, z))),
                      lerp(v, lerp(u, grad(perm[AA + 1], x, y, z - 1), grad(perm[BA + 1], x - 1, y, z - 1)),
                           lerp(u, grad(perm[AB + 1], x, y - 1, z - 1), grad(perm[BB + 1], x - 1, y - 1, z - 1))));
    return (res + 1.0) / 2.0;
}

double Perlin::fade(double t) {
    return t * t * t * (t * (t * 6 - 15) + 10);
}

double Perlin::lerp(double t, double a, double b) {
    return a + t * (b - a);
}

double Perlin::grad(int32_t hash, double x, double y, double z) {
    int32_t h = hash & 15;
    // Convert lower 4 bits of hash into 12 gradient directions
    double u = h < 8 ? x : y;
    double v = h < 4 ? y : ((h == 12 || h == 14) ? x : z);
    return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}
