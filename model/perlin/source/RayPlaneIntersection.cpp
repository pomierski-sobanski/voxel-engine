//
// Created by Michał Sobański on 27.09.2021.
//

#include "../../../configs/defines.h"
#include "../header/RayPlaneIntersection.h"

std::pair<float, faceType> RayPlaneIntersection::nearestIntersectedFace(Block *block,
                                                                        const float blockXYZ[3],
                                                                        const float rayPointXYZ[3],
                                                                        const float rayVectorXYZ[3]) {
    Ray r(rayVectorXYZ, rayPointXYZ);
    std::vector<faceType> *faceTypes = new std::vector<faceType>;
    blockToFaces(faceTypes, block);
    std::pair<float, faceType> nearestFace = {GRASP_RADIUS + 1.0f, UINT8_MAX};
    for (auto it = faceTypes->cbegin(); it != faceTypes->cend(); ++it) {
        const faceType faceType = *it;
        Point faceCenter(Point(blockXYZ).toFaceCenter(faceType));
        Point translated(faceCenter.translate(faceType));
        Plane plane(Vector(translated - faceCenter), faceCenter);
        std::pair<bool, Point> intersectionPoint = plane.getIntersectionPoint(r);
        if (!intersectionPoint.first) continue;
        const float distance = intersectionPoint.second.distanceBetween(Point(rayPointXYZ));
        if (distance <= GRASP_RADIUS && distance < nearestFace.first
            && faceCenter.toFaceRanges().isInRange(&intersectionPoint.second)) {
            nearestFace.first = distance;
            nearestFace.second = faceType;
        }
    }
    delete faceTypes;
    return nearestFace;
}

void RayPlaneIntersection::blockToFaces(std::vector<faceType> *faceTypes,
                                        Block *block) {
    if (block->up == 0x1) faceTypes->push_back(UP);
    if (block->down == 0x1) faceTypes->push_back(DOWN);
    if (block->north == 0x1) faceTypes->push_back(NORTH);
    if (block->east == 0x1) faceTypes->push_back(EAST);
    if (block->west == 0x1) faceTypes->push_back(WEST);
    if (block->south == 0x1) faceTypes->push_back(SOUTH);
}

std::pair<bool, RayPlaneIntersection::Point>
RayPlaneIntersection::Plane::getIntersectionPoint(const Ray &ray) {
    int64_t i;
    for (i = 0; i < 3; i++) if (vec[i] != 0) break;
    if (ray.vec[i] != 0 && std::signbit(pt[i] - ray.pt[i]) == std::signbit(ray.vec[i])) {
        const float t = (pt[i] - ray.pt[i]) / ray.vec[i];
        float intersectionPoint[3] = {0.0f, 0.0f, 0.0f};
        for (i = 0; i < 3; i++) intersectionPoint[i] = ray.pt[i] + (ray.vec[i] * t);
        return {true, Point(intersectionPoint)};
    } else if (pt[i] == ray.pt[i]) {
        return {true, Point(pt)};
    } else {
        return {false, Point(0.0f, 0.0f, 0.0f)};
    }
}

RayPlaneIntersection::Ranges RayPlaneIntersection::Point::toFaceRanges() const {
    Ranges ranges;
    /* x */
    ranges.xRange.first = x - 0.5f;
    ranges.xRange.second = x + 0.5f;
    /* y */
    ranges.yRange.first = y - 0.5f;
    ranges.yRange.second = y + 0.5f;
    /* z */
    ranges.zRange.first = z - 0.5f;
    ranges.zRange.second = z + 0.5f;
    return ranges;
}

RayPlaneIntersection::Point RayPlaneIntersection::Point::toFaceCenter(const faceType faceType) const {
    switch (faceType) {
        case UP:
            return {(x + 0.5f), (y + 0.5f), (z + 1.0f)};
        case DOWN:
            return {(x + 0.5f), (y + 0.5f), z};
        case NORTH:
            return {(x + 0.5f), (y + 1.0f), (z + 0.5f)};
        case SOUTH:
            return {(x + 0.5f), y, (z + 0.5f)};
        case EAST:
            return {(x + 1.0f), (y + 0.5f), (z + 0.5f)};
        case WEST:
        default:
            return {x, (y + 0.5f), (z + 0.5f)};
    }
}

RayPlaneIntersection::Point RayPlaneIntersection::Point::translate(const faceType faceType) const {
    switch (faceType) {
        case DOWN:
            return {x, y, DOWN_BLOCK(z)};
        case UP:
            return {x, y, UP_BLOCK(z)};
        case SOUTH:
            return {x, SOUTH_BLOCK(y), z};
        case NORTH:
            return {x, NORTH_BLOCK(y), z};
        case WEST:
            return {WEST_BLOCK(x), y, z};
        case EAST:
        default:
            return {EAST_BLOCK(x), y, z};
    }
}

RayPlaneIntersection::Point::Point(float x, float y, float z) : x(x), y(y), z(z) {}

RayPlaneIntersection::Point::Point(RayPlaneIntersection::Point &p) = default;

RayPlaneIntersection::Point::Point(RayPlaneIntersection::Point &&p) noexcept = default;

RayPlaneIntersection::Point::Point(const float xyz[3]) : Point(xyz[0], xyz[1], xyz[2]) {}

RayPlaneIntersection::Point RayPlaneIntersection::Point::operator-(const RayPlaneIntersection::Point &p) {
    return {x - p.x, y - p.y, z - p.z};
}

float RayPlaneIntersection::Point::distanceBetween(const RayPlaneIntersection::Point &p) {
    Point point(*this - p);
    return std::sqrt(sq(point.x) + sq(point.y) + sq(point.z));
}

RayPlaneIntersection::Vector::Vector(RayPlaneIntersection::Point &&p) : Point(p) {}

RayPlaneIntersection::Ray::Ray(const float vector[3], const float point[3])
        : vec{vector[0], vector[1], vector[2]}, pt{point[0], point[1], point[2]} {}

RayPlaneIntersection::Ray::Ray(Vector &vec, Point &pt)
        : vec{vec.x, vec.y, vec.z}, pt{pt.x, pt.y, pt.z} {}

RayPlaneIntersection::Plane::Plane(Vector &&vec, Point &pt) : Ray(vec, pt) {}
