//
// Created by Michał Sobański on 01.09.2021.
//

#include "../header/Biome.h"
#include "../header/biomes/IceBiome.h"
#include "../header/biomes/WaterBiome.h"
#include "../header/biomes/BeachBiome.h"
#include "../header/biomes/DesertBiome.h"
#include "../header/biomes/PlainsBiome.h"
#include "../header/biomes/ForestBiome.h"
#include "../header/biomes/TaigaBiome.h"
#include "../header/biomes/TundraBiome.h"

std::array<Biome *, Biome::NUM_OF_BIOMES> Biome::biomes = {nullptr};

const Biome &&Biome::iceBiome = IceBiome();

const Biome &&Biome::waterBiome = WaterBiome();

const Biome &&Biome::beachBiome = BeachBiome();

const Biome &&Biome::desertBiome = DesertBiome();

const Biome &&Biome::plainsBiome = PlainsBiome();

const Biome &&Biome::forestBiome = ForestBiome();

const Biome &&Biome::taigaBiome = TaigaBiome();

const Biome &&Biome::tundraBiome = TundraBiome();

Biome::Biome(Biome::Id id, double coeffMin, double coeffMax, Biome::PlantsType plantsType,
             std::array<BlockType::Id, LAYERS_ARR_LEN> &&layers)
        : id(id), coeffMin(coeffMin), coeffMax(coeffMax),
          plantsType(plantsType), layers(layers) {
    biomes[id] = this;
}

bool Biome::operator==(const Biome &rhs) const {
    return id == rhs.id;
}

bool Biome::operator!=(const Biome &rhs) const {
    return id != rhs.id;
}

Biome *Biome::getBiome(uint8_t biomeId) {
    return getBiome(static_cast<Biome::Id>(biomeId));
}

Biome *Biome::getBiome(Biome::Id biomeId) {
    return biomes[biomeId];
}

Biome::PlantsType Biome::getPlantsType(uint8_t biomeId) {
    return getBiome(biomeId)->plantsType;
}

Biome::PlantsType Biome::getPlantsType(Biome::Id biomeId) {
    return getBiome(biomeId)->plantsType;
}

const Biome::Id Biome::getId() const {
    return id;
}

Biome::Id Biome::getBiomeId(uint32_t z, double coeff) {
    Biome::Id biomeId = Biome::TUNDRA;
    for (uint8_t id = 0; id < Biome::NUM_OF_BIOMES; id++) {
        if (coeff >= (*Biome::biomes[id]).coeffMin && coeff <= (*Biome::biomes[id]).coeffMax) {
            biomeId = static_cast<Biome::Id>(id);
            break;
        }
    }

    if (z >= ICE_MIN_Z && z <= ICE_MAX_Z && biomeId == Biome::TUNDRA)
        return Biome::ICE;
    if (z <= SEA_LEVEL)
        return Biome::WATER;
    if (z >= BEACH_MIN_Z && z <= BEACH_MAX_Z && biomeId != Biome::TUNDRA)
        return Biome::BEACH;
    if (z >= PLAINS_MIN_Z && z <= PLAINS_MAX_Z && biomeId != Biome::TUNDRA && biomeId != Biome::DESERT)
        return Biome::PLAINS;

    return biomeId;
}

const BlockType *Biome::getBlockType(uint32_t z, uint32_t maxZ) const {
    Biome *biome = Biome::biomes[id];
    if (z == 0) return BlockType::getBlockType(biome->layers[BEDROCK_LAYER_INDEX]);
    if (z == maxZ) return BlockType::getBlockType(biome->layers[SURFACE_LAYER_INDEX]);
    if (maxZ < LAYERS_ARR_LEN) {
        return BlockType::getBlockType(biome->layers[z]);
    }
    /* SURFACE LAYER > z > BEDROCK LAYER */
    const double layerHeight = ((double) maxZ) / (LAYERS_ARR_LEN - 2.0);
    int index = LAYERS_ARR_LEN - 2;
    for (double h = 0.0; (h <= maxZ) && (index >= 1); h += layerHeight, index -= 1) {
        if (z >= h && z <= h + layerHeight) return BlockType::getBlockType(biome->layers[index]);
    }
    return BlockType::getBlockType(biome->layers[BEDROCK_LAYER_INDEX]);
}

void Biome::calcFaces(BiomeFragment *frag) {
    Faces faces;
    for (uint32_t z = *frag->minZ; z <= *frag->maxZ; z++) {
        faces.type = frag->centralB->getBlockType(z, *frag->maxZ);
        faces.up = (z == *frag->centralZ);
        faces.down = false;
        faces.north = (*frag->nZ < z);
        faces.west = (*frag->wZ < z);
        faces.south = (*frag->sZ < z);
        faces.east = (*frag->eZ < z);
        addBlock(frag, &z, &faces);
    }
}

void Biome::addBlock(BiomeFragment *frag, uint32_t *z, Faces *faces) {
    if (faces->up || faces->down || faces->north
        || faces->west || faces->south || faces->east) {
        Block block;
        block.x = CHUNK_X_BITMASK((*frag->x - 1) % 16);
        block.y = CHUNK_Y_BITMASK((*frag->y - 1) % 16);
        block.z = CHUNK_Z_BITMASK(*z);
        block.up = faces->up & 0x1;
        block.down = faces->down & 0x1;
        block.north = faces->north & 0x1;
        block.west = faces->west & 0x1;
        block.south = faces->south & 0x1;
        block.east = faces->east & 0x1;
        block.blockType = faces->type->getId() & 0xff;
        frag->blocks->at(block.blockType)->push_back(block);
    }
}
