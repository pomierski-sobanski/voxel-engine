//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Water.h"

Water::Water() : BlockType(WATER, {
        {
                {0.2, 0.0}, //top
                {0.2, 0.0}, //side
                {0.2, 0.0}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}

bool Water::isSolid() const {
    return false;
}

bool Water::canBeBroken() const {
    return false;
}
