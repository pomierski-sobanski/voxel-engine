//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Planks.h"

Planks::Planks() : BlockType(PLANKS, {
        {
                {0.6, 0.4}, //top
                {0.6, 0.4}, //side
                {0.6, 0.4}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
