//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Grass.h"

Grass::Grass() : BlockType(GRASS, {
        {
                {-1.0, -1.0}, //top
                {0.2, 0.6}, //side
                {-1.0, -1.0}  //bottom
        },
        0, //alpha
        16, //width
        true //diagonal
}) {}

bool Grass::isSolid() const {
    return false;
}
