//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/GrassBlock.h"

GrassBlock::GrassBlock() : BlockType(GRASS_BLOCK, {
        {
                {0.6, 0.6}, //top
                {0.4, 0.6}, //side
                {0.0, 0.6}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
