//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Bedrock.h"

Bedrock::Bedrock() : BlockType(BEDROCK, {
        {
                {0.0, 0.8}, //top
                {0.0, 0.8}, //side
                {0.0, 0.8}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}

bool Bedrock::canBeBroken() const {
    return false;
}
