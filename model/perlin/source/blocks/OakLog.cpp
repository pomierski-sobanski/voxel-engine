//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/OakLog.h"

OakLog::OakLog() : BlockType(OAK_LOG, {
        {
                {0.0, 0.2}, //top
                {0.2, 0.4}, //side
                {0.0, 0.2}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
