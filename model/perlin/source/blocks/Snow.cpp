//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Snow.h"

Snow::Snow() : BlockType(SNOW, {
        {
                {0.0, 0.0}, //top
                {0.0, 0.0}, //side
                {0.0, 0.0}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
