//
// Created by Michał Sobański on 15.09.2021.
//

#include "../../header/blocks/PineNeedles.h"

PineNeedles::PineNeedles() : BlockType(PINE_NEEDLES, {
        {
                {0.4, 0.2}, //top
                {0.4, 0.2}, //side
                {0.4, 0.2}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
