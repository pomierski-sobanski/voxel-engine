//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Cobblestone.h"

Cobblestone::Cobblestone() : BlockType(COBBLESTONE, {
        {
                {0.6, 0.8}, //top
                {0.6, 0.8}, //side
                {0.6, 0.8}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}
