//
// Created by Michał Sobański on 05.09.2021.
//

#include "../../header/blocks/Air.h"

Air::Air() : BlockType(AIR, {
        {
                {-1.0, -1.0}, //top
                {-1.0, -1.0}, //side
                {-1.0, -1.0}  //bottom
        },
        0, //alpha
        16, //width
        false //diagonal
}) {}

bool Air::isSolid() const {
    return false;
}

bool Air::canBeBroken() const {
    return false;
}
