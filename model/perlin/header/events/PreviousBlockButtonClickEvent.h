//
// Created by pawel on 05.12.2021.
//

#ifndef VOXEL_ENGINE_PREVIOUSBLOCKBUTTONCLICKEVENT_H
#define VOXEL_ENGINE_PREVIOUSBLOCKBUTTONCLICKEVENT_H

#include "MouseButtonClickEvent.h"

class PreviousBlockButtonClickEvent : public PerlinEvent {
public:
    void handle(Perlin *perlin) override;

};


#endif //VOXEL_ENGINE_PREVIOUSBLOCKBUTTONCLICKEVENT_H
