//
// Created by Michał Sobański on 03.10.2021.
//

#ifndef VOXEL_ENGINE_PERLINEVENT_H
#define VOXEL_ENGINE_PERLINEVENT_H

#include "../Perlin.h"

class PerlinEvent {
public:
    virtual void handle(Perlin *perlin);
};


#endif //VOXEL_ENGINE_PERLINEVENT_H
