//
// Created by Michał Sobański on 03.10.2021.
//

#ifndef VOXEL_ENGINE_LEFTMOUSEBUTTONCLICKEVENT_H
#define VOXEL_ENGINE_LEFTMOUSEBUTTONCLICKEVENT_H


#include "MouseButtonClickEvent.h"

class LeftMouseButtonClickEvent : public MouseButtonClickEvent {
protected:
    bool isPlaceBlockEvent() override;

    void modifyBlocks(Perlin *perlin, const NearestBlocks &nearestBlocks) override;
};


#endif //VOXEL_ENGINE_LEFTMOUSEBUTTONCLICKEVENT_H
