//
// Created by Michał Sobański on 12.09.2021.
//

#ifndef VOXEL_ENGINE_PERLINPLANTS_H
#define VOXEL_ENGINE_PERLINPLANTS_H

#define MAX_ATTEMPTS 12

#define MIN_NUM_OF_TREES 2
#define MAX_NUM_OF_TREES 4

#define MIN_NUM_OF_GRASSES 30
#define MAX_NUM_OF_GRASSES 60

#define CACTUS_PROBABILITY 0.39
#define CACTUS_HEIGHT 3

#define SPACE_BETWEEN_TREES 3
#define SPACE_BETWEEN_CACTUSES 1
#define SPACE_BETWEEN_GRASSES 0

#define MIN_SHAPE_COEFF 0.7 // TREES
#define MAX_SHAPE_COEFF 1.0 // TREES
#define MIN_TRUNK_HEIGHT 6 // TREES
#define MAX_TRUNK_HEIGHT 29 // TREES
#define MIN_RADIUS 3 // TREES
#define MAX_RADIUS 4 // TREES
#define MIN_LEAVES_START 4 // TREES
#define MAX_LEAVES_START 12 // TREES
#define MIN_LEAVES_HEIGHT 9 // TREES

#include "../header/Biome.h"
#include "../header/PerlinChunk.h"
#include "../../rng/header/SimpleRng.h"

#include <cstdint>
#include <vector>
#include <bitset>

class PerlinPlants {
public:
    ~PerlinPlants();

    PerlinPlants(const ChunkId *id, const uint64_t *worldSeed, uint8_t **biome,
                 uint32_t **elevation, std::array<std::vector<Block> *, BlockType::Id::NUM_OF_BLOCK_TYPES> *blocks);

    void generatePlants();

private:
    static const double pi;
    const ChunkId *id;
    const uint64_t *worldSeed;
    uint8_t **biome;
    uint32_t **elevation;
    std::array<std::vector<Block> *, BlockType::Id::NUM_OF_BLOCK_TYPES> *blocks;
    SimpleRng *rng;
    std::bitset<MAX_BLOCKS> *plantsBlocks;
    std::bitset<MAX_BLOCKS> *leavesBlocks;
    std::bitset<CHUNK_SIZE * CHUNK_SIZE> *freeFields;
    double shapeCoeff;
    int64_t leavesStart;
    int64_t radius;
    int64_t trunkHeight;
    int64_t leavesHeight;

    void addTree(int16_t x, int16_t y);

    void addCactus(int16_t x, int16_t y);

    void addGrass(int16_t x, int16_t y);

    uint8_t getNumOfPlants(uint8_t plantsType, uint16_t numOfBlocks);

    static double leavesFunc(double x, double shapeCoeff, double leavesHeight);

    static uint8_t getLeavesType(uint8_t biomeId);

    static uint8_t getTrunkType(uint8_t biomeId);
};


#endif //VOXEL_ENGINE_PERLINPLANTS_H
