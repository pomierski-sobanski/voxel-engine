//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_STONE_H
#define VOXEL_ENGINE_STONE_H

#include "../BlockType.h"

class Stone : public BlockType {
public:
    Stone();
};


#endif //VOXEL_ENGINE_STONE_H
