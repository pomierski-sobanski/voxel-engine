//
// Created by Michał Sobański on 15.09.2021.
//

#ifndef VOXEL_ENGINE_PINENEEDLES_H
#define VOXEL_ENGINE_PINENEEDLES_H

#include "../BlockType.h"

class PineNeedles : public BlockType {
public:
    PineNeedles();
};


#endif //VOXEL_ENGINE_PINENEEDLES_H
