//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_BEDROCK_H
#define VOXEL_ENGINE_BEDROCK_H

#include "../BlockType.h"

class Bedrock : public BlockType {
public:
    Bedrock();

    bool canBeBroken() const override;
};


#endif //VOXEL_ENGINE_BEDROCK_H
