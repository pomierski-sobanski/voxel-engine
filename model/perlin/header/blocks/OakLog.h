//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_OAKLOG_H
#define VOXEL_ENGINE_OAKLOG_H

#include "../BlockType.h"

class OakLog : public BlockType {
public:
    OakLog();
};


#endif //VOXEL_ENGINE_OAKLOG_H
