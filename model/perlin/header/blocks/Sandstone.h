//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_SANDSTONE_H
#define VOXEL_ENGINE_SANDSTONE_H

#include "../BlockType.h"

class Sandstone : public BlockType {
public:
    Sandstone();
};


#endif //VOXEL_ENGINE_SANDSTONE_H
