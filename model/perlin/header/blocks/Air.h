//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_AIR_H
#define VOXEL_ENGINE_AIR_H

#include "../BlockType.h"

class Air : public BlockType {
public:
    Air();

    bool isSolid() const override;

    bool canBeBroken() const override;
};


#endif //VOXEL_ENGINE_AIR_H
