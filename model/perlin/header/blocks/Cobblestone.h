//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_COBBLESTONE_H
#define VOXEL_ENGINE_COBBLESTONE_H

#include "../BlockType.h"

class Cobblestone : public BlockType {
public:
    Cobblestone();
};


#endif //VOXEL_ENGINE_COBBLESTONE_H
