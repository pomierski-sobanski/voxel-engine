//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_OAKLEAVES_H
#define VOXEL_ENGINE_OAKLEAVES_H

#include "../BlockType.h"

class OakLeaves : public BlockType {
public:
    OakLeaves();
};


#endif //VOXEL_ENGINE_OAKLEAVES_H
