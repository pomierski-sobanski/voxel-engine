//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_ICE_H
#define VOXEL_ENGINE_ICE_H

#include "../BlockType.h"

class Ice : public BlockType {
public:
    Ice();

    bool canBeBroken() const override;
};


#endif //VOXEL_ENGINE_ICE_H
