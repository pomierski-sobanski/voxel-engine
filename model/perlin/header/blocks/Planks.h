//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_PLANKS_H
#define VOXEL_ENGINE_PLANKS_H

#include "../BlockType.h"

class Planks : public BlockType {
public:
    Planks();
};


#endif //VOXEL_ENGINE_PLANKS_H
