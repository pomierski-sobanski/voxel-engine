//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_WATER_H
#define VOXEL_ENGINE_WATER_H

#include "../BlockType.h"

class Water : public BlockType {
public:
    Water();

    bool isSolid() const override;

    bool canBeBroken() const override;
};


#endif //VOXEL_ENGINE_WATER_H
