//
// Created by Michał Sobański on 06.08.2021.
//

#ifndef VOXEL_ENGINE_PERLINCHUNK_H
#define VOXEL_ENGINE_PERLINCHUNK_H

#include "../../../configs/defines.h"
#include "BlockType.h"

#define ELEVATION_ARR_SIZE CHUNK_SIZE + 2
#define CHUNK_ARR_REAL_X(x) ((x) + 1)
#define CHUNK_ARR_REAL_Y(y) ((y) + 1)

typedef std::array<Block *, CHUNK_HEIGHT> zArr;
typedef std::array<zArr, CHUNK_SIZE> yzArr;
typedef std::array<yzArr, CHUNK_SIZE> xyzArr;

#include <iostream>
#include <cstdint>
#include <atomic>
#include <cmath>

class Faces;

class ChunkId {
public:
    uint32_t x = 0;
    uint32_t y = 0;

    friend bool operator==(const ChunkId &l, const ChunkId &r) {
        return l.x == r.x && l.y == r.y;
    }

    bool operator<(const ChunkId &id) const {
        return x < id.x || (x == id.x && y < id.y);
    }

    ChunkId operator-(ChunkId const &obj) {
        ChunkId res;
        res.x = (x > obj.x) ? x - obj.x : obj.x - x;
        res.y = (y > obj.y) ? y - obj.y : obj.y - y;
        return res;
    }

    double length() {
        return sqrt(pow(x, 2) + pow(y, 2));
    }
};

class PerlinChunk {
public:
    PerlinChunk(const ChunkId &id, const uint32_t &worldSeed);

    virtual ~PerlinChunk();

    const ChunkId *getId() const;

    const std::array<std::vector<Block> *, BlockType::Id::NUM_OF_BLOCK_TYPES> *
    getBlocks() const;

    const std::array<uint32_t, BlockType::Id::NUM_OF_BLOCK_TYPES> *
    getFacesSum() const;

    uint8_t ***getBlocksArr3D() const;

    Block *getBlock(uint8_t blockType, uint32_t x,
                    uint32_t y, uint32_t z) const;

    const std::atomic<bool> &getChangedFlag() const;

    void setChangedFlag(bool changed);

    uint32_t getNumOfFaces() const;

    uint32_t getCornerX() const;

    uint32_t getCornerY() const;

    const ChunkId &getChunkId() const;

    void setElevation(uint8_t x, uint8_t y, uint32_t z);

    void setBiome(uint8_t x, uint8_t y, uint8_t biomeId);

    void calcFaces();

    void sumFaces();

    void setBlocksArr3D();

    void addPlants();

    void addNewBlock(Block block);

    void breakBlock(Block *block);

    void placeBlock(Block block);

    void printElevation();

    void printBiome();

    void printBlocks();

    static bool isBelowSeaLevel(float z);

    static uint32_t getBitPosition(uint32_t x, uint32_t y);

    static uint32_t getBitPosition(uint32_t x, uint32_t y, uint32_t z);

private:
    const ChunkId id;
    const uint64_t worldSeed;
    const uint32_t cornerX;
    const uint32_t cornerY;
    const uint32_t x0;
    const uint32_t y0;
    uint32_t numOfFaces = 0;
    uint8_t ***blocksArr3D; //[x][y][z]
    std::array<std::vector<Block> *, BlockType::Id::NUM_OF_BLOCK_TYPES> *blocks;
    std::array<uint32_t, BlockType::Id::NUM_OF_BLOCK_TYPES> *facesSum;
    std::atomic<bool> changed;
    uint32_t **elevation;
    uint8_t **biome;
};


#endif //VOXEL_ENGINE_PERLINCHUNK_H
