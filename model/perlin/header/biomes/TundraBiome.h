//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_TUNDRABIOME_H
#define VOXEL_ENGINE_TUNDRABIOME_H

#include "../Biome.h"

class TundraBiome : public Biome {
public:
    TundraBiome();
};


#endif //VOXEL_ENGINE_TUNDRABIOME_H
