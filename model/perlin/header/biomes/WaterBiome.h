//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_WATERBIOME_H
#define VOXEL_ENGINE_WATERBIOME_H

#include "../Biome.h"

class WaterBiome : public Biome {
public:
    WaterBiome();

    void calcFaces(BiomeFragment *frag) override;
};


#endif //VOXEL_ENGINE_WATERBIOME_H
