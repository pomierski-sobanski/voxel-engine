//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_ICEBIOME_H
#define VOXEL_ENGINE_ICEBIOME_H

#include "../Biome.h"

class IceBiome : public Biome {
public:
    IceBiome();

    void calcFaces(BiomeFragment *frag) override;
};


#endif //VOXEL_ENGINE_ICEBIOME_H
