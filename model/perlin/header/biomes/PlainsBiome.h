//
// Created by Michał Sobański on 05.09.2021.
//

#ifndef VOXEL_ENGINE_PLAINSBIOME_H
#define VOXEL_ENGINE_PLAINSBIOME_H

#include "../Biome.h"

class PlainsBiome : public Biome {
public:
    PlainsBiome();
};


#endif //VOXEL_ENGINE_PLAINSBIOME_H
