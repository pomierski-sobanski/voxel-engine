//
// Created by Michał Sobański on 01.09.2021.
//

#ifndef VOXEL_ENGINE_BIOME_H
#define VOXEL_ENGINE_BIOME_H

#define LAYERS_ARR_LEN 5  // >2 (!)
#define SURFACE_LAYER_INDEX 0
#define BEDROCK_LAYER_INDEX (LAYERS_ARR_LEN - 1)

#define NOT_APPLICABLE  -1

#define SEA_LEVEL 20

#define ICE_MIN_Z SEA_LEVEL - 5
#define ICE_MAX_Z SEA_LEVEL

#define BEACH_MIN_Z SEA_LEVEL + 1
#define BEACH_MAX_Z BEACH_MIN_Z + 1

#define PLAINS_MIN_Z BEACH_MAX_Z + 1
#define PLAINS_MAX_Z PLAINS_MIN_Z + 1

#define DESERT_MIN  0.000
#define DESERT_MAX  0.435

#define PLAINS_MIN  DESERT_MAX
#define PLAINS_MAX  0.470

#define FOREST_MIN  PLAINS_MAX
#define FOREST_MAX  0.515

#define TAIGA_MIN   FOREST_MAX
#define TAIGA_MAX   0.575

#define TUNDRA_MIN  TAIGA_MAX
#define TUNDRA_MAX  1.000

#include "../header/BlockType.h"

class BiomeFragment;

typedef struct Faces {
    const BlockType *type;
    bool up;
    bool down;
    bool north;
    bool west;
    bool south;
    bool east;
} Faces;

class Biome {
public:
    enum Id : uint8_t {
        ICE,
        WATER,
        BEACH,
        DESERT,
        PLAINS,
        FOREST,
        TAIGA,
        TUNDRA,
        NUM_OF_BIOMES
    };
    enum PlantsType : uint8_t {
        NO_PLANTS,
        TREES,
        CACTUSES,
        GRASSES,
        NUM_OF_PLANTS_TYPES
    };

    bool operator==(const Biome &rhs) const;

    bool operator!=(const Biome &rhs) const;

    const Id getId() const;

    virtual void calcFaces(BiomeFragment *frag);

    const BlockType *getBlockType(uint32_t z, uint32_t maxZ) const;

    static Biome *getBiome(uint8_t biomeId);

    static Biome *getBiome(Biome::Id biomeId);

    static Biome::Id getBiomeId(uint32_t z, double coeff);

    static PlantsType getPlantsType(uint8_t biomeId);

    static PlantsType getPlantsType(Biome::Id biomeId);

protected:
    static std::array<Biome *, Biome::NUM_OF_BIOMES> biomes;
    static const Biome &&iceBiome;
    static const Biome &&waterBiome;
    static const Biome &&beachBiome;
    static const Biome &&desertBiome;
    static const Biome &&plainsBiome;
    static const Biome &&forestBiome;
    static const Biome &&taigaBiome;
    static const Biome &&tundraBiome;
    const Biome::Id id;
    const double coeffMin;
    const double coeffMax;
    const Biome::PlantsType plantsType;
    const std::array<BlockType::Id, LAYERS_ARR_LEN> layers; // { surface -> below ground level -> bedrock }

    Biome(Biome::Id id, double coeffMin, double coeffMax, Biome::PlantsType plantsType,
          std::array<BlockType::Id, LAYERS_ARR_LEN> &&layers);

    static void addBlock(BiomeFragment *frag, uint32_t *z, Faces *faces);
};

typedef struct BiomeFragment {
    uint8_t *x;
    uint8_t *y;
    std::array<std::vector<Block> *, BlockType::Id::NUM_OF_BLOCK_TYPES> *blocks;
    uint32_t *centralZ; //central z
    uint32_t *eZ; // east z
    uint32_t *nZ; // north z
    uint32_t *wZ; // west z
    uint32_t *sZ; // south z
    uint32_t *minZ; // minimum z
    uint32_t *maxZ; // maximum z
    Biome *centralB; // central biome
    Biome *eB; // east biome
    Biome *nB; // north biome
    Biome *wB; // west biome
    Biome *sB; // south biome
} BiomeFragment;

#endif //VOXEL_ENGINE_BIOME_H
