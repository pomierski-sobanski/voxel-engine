#version 330 core
out vec4 FragColor;
in vec2 v_TexCoord;
in vec3 v_Normal;

uniform vec3 u_LightDir;
uniform vec3 u_LightColor;
uniform sampler2D u_Texture;

void main()
{
    vec4 texColor = texture(u_Texture, v_TexCoord);
    if(texColor.a < 0.2)
        discard;

    float ambientStrength = 0.6;
    vec3 ambient = ambientStrength * u_LightColor;

    vec3 norm = normalize(v_Normal);

    float diffuseStrength = 0.4;
    float diff = max(dot(norm, u_LightDir), 0.0) * diffuseStrength;
    vec3 diffuse = diff * u_LightColor;

    vec3 result = (ambient + diffuse) * texColor.rgb;

    FragColor = vec4(result, texColor.a);
}