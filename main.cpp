#include <iostream>
#include "./model/header/World.h"
#include "model/header/JsonConfig.h"

int main(int argc, char **argv) {
    if (!glfwInit())
        return -1;

    int seed;
    if (argc == 2) {
        seed = atoi(argv[1]);
    } else {
        seed = 672482;
    }
    std::cout << "seed: " << seed << std::endl;
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Initialize the library */
    Screen *screen = new Screen(JsonConfig::getInstance()->getHeight(), JsonConfig::getInstance()->getWidth(),
                                JsonConfig::getInstance()->getTitle());

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
        return -1;
    //HERE YOU CAN START USING OPENGL FUNCTIONS

    std::cout << glGetString(GL_VERSION) << std::endl;

    World *world = new World(screen, seed);

//    {   //EXAMPLE
//        world->perlinInput.enqueue({1, 1});
//        PerlinChunk *perlinChunk = world->perlinOutput.dequeueAndWait();
//        perlinChunk->printElevation();
//        perlinChunk->printBiome();
//        perlinChunk->printBlocks();
//
//        Perlin perlin = Perlin(world);
//        uint32_t numOfChunks = 75000;
//        perlin.printFacesStats(numOfChunks);
//    }

    world->start(); //Shouldn't it work in separate thread?

    glfwTerminate();

    delete world;
    world = nullptr;

    return 0;
}