//
// Created by pawel on 24.08.2021.
//

#ifndef VOXEL_ENGINE_SCREEN_H
#define VOXEL_ENGINE_SCREEN_H

#ifndef GLFW_H
#define GLFW_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#endif //GLFW_H

#include <string>

class Screen {
public:
    Screen(uint32_t height, uint32_t width, std::string &&title);

    float getRatio();

    GLFWwindow *getWindow();

    int getHeight() const;

    void setHeight(int height);

    int getWidth() const;

    void setWidth(int width);

private:
    GLFWwindow *window;
    uint32_t height;
    uint32_t width;
};


#endif //VOXEL_ENGINE_SCREEN_H
